<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index')->name('index');
Route::get('/search', 'WebsiteController@search')->name('search');
Route::get('/products', 'WebsiteController@products')->name('products');
Route::get('/bloggers', 'WebsiteController@pages')->name('pages');
Route::get('/blogger/{id}', 'WebsiteController@page')->name('page');
Route::get('/blogger/{id}/posts', 'WebsiteController@pageSocial')->name('page.posts');
Route::get('/subscribe', 'WebsiteController@pageSubscribe')->name('page.subscribe');
Route::get('/subscribes/get', 'WebsiteController@subscribes')->name('page.subscribes');
Route::get('/cart/add', 'CartController@add')->name('cart.add');
Route::get('/favorites/add', 'CartController@favorites')->name('favorites.add');
Route::get('/modal-product', 'WebsiteController@modalProduct')->name('modal.product');
Route::get('/cart', 'CartController@index')->name('cart');
Route::get('/cart/count', 'CartController@count')->name('cart.count');
Route::match(['get', 'post'], '/checkout', 'CartController@checkout')->name('checkout');
Route::match(['get', 'post'], '/checkout/thanks/{id?}', 'CartController@checkoutThanks')->name('checkout.thanks');
Route::get('/about', 'WebsiteController@about')->name('about');
Route::get('/feedback', 'FeedbackController@index')->name('feedback');
Route::post('/feedback/add', 'FeedbackController@add')->name('feedback.add');
Route::get('/contacts', 'WebsiteController@contacts')->name('contacts');
Route::get('/questions', 'WebsiteController@questions')->name('questions');
Route::get('/job', 'WebsiteController@job')->name('job');
Route::get('/return', 'WebsiteController@return')->name('return');


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'isAdmin']], function(){

    Route::get('/', 'Admin\AdminController@index')->name('admin');

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'Admin\UserController@list')->name('admin.users');
        Route::get('/{id}', 'Admin\UserController@user')->name('admin.user');
        Route::match(['get', 'post'], '/edit/{id}', 'Admin\UserController@edit')->name('admin.user.edit');
        Route::get('/{id}/delete', 'Admin\UserController@delete')->name('admin.user.delete');
        Route::post('/{id}/balance/add', 'Admin\UserController@userAddBalance')->name('admin.user.balance.add');
    });

    Route::group(['prefix' => 'pages'], function () {
        Route::get('/', 'Admin\PageController@list')->name('admin.pages');
        Route::match(['get', 'post'], '/edit/{id?}', 'Admin\PageController@edit')->name('admin.page.edit');
        Route::get('/{id}/delete', 'Admin\PageController@delete')->name('admin.page.delete');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'Admin\CategoryController@list')->name('admin.categories');
        Route::match(['get', 'post'], '/edit/{id?}', 'Admin\CategoryController@edit')->name('admin.category.edit');
        Route::get('/{id}/delete', 'Admin\CategoryController@delete')->name('admin.category.delete');
    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'Admin\ProductController@list')->name('admin.products');
        Route::match(['get', 'post'], '/edit/{id?}', 'Admin\ProductController@edit')->name('admin.product.edit');
        Route::get('/{id}/delete', 'Admin\ProductController@delete')->name('admin.product.delete');
        Route::post('/{id}/add-image', 'Admin\ProductController@addImage')->name('admin.product.addImage');
        Route::post('/delete-image/{id?}', 'Admin\ProductController@deleteImage')->name('admin.product.deleteImage');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'Admin\OrderController@list')->name('admin.orders');
        Route::match(['get', 'post'], '/edit/{id}', 'Admin\OrderController@edit')->name('admin.order.edit');
        Route::get('/print/{id}', 'Admin\OrderController@print')->name('admin.order.print');
    });

    Route::group(['prefix' => 'withdraw-requests'], function () {
        Route::get('/', 'Admin\WithdrawRequestController@list')->name('admin.withdrawRequests');
        Route::match(['get', 'post'], '/{id}/edit', 'Admin\WithdrawRequestController@edit')->name('admin.withdrawRequest.edit');
    });

    Route::group(['prefix' => 'feedbacks'], function () {
        Route::get('/', 'FeedbackController@list')->name('admin.feedbacks');
        Route::match(['get', 'post'], '/{id}/edit', 'FeedbackController@edit')->name('admin.feedback.edit');
    });

    Route::group(['prefix' => 'slides'], function () {
        Route::get('/', 'WebsiteController@slides')->name('admin.slides');
        Route::match(['get', 'post'], '/edit/{id?}', 'WebsiteController@slideEdit')->name('admin.slide.edit');
        Route::get('/{id}/delete', 'WebsiteController@slideDelete')->name('admin.slide.delete');
    });

});

Auth::routes();

Route::group(['prefix' => 'home', 'middleware' => ['auth', 'isUser']], function() {

    Route::get('/', 'HomeController@index')->name('user.home');

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'User\ProductController@list')->name('user.products');
        Route::post('/change-price', 'User\ProductController@changePrice')->name('user.products.changePrice');
    });

    Route::group(['prefix' => 'wallet'], function () {
        Route::get('/', 'User\WalletController@index')->name('user.wallet');
        Route::post('/Withdraw-requests/add', 'User\WalletController@makeWithdrawRequest')->name('user.wallet.withdrawRequests.add');
    });

});
