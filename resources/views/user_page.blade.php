@extends('layouts.app')

@section('title') {{ $page->footer_title }} | BloggerStore @endsection

@section('content')
  <section class="set-bg user-page-big-image" data-setbg="{{ $page->header_image }}"></section>
  <section class="container pb-5 mb-5">
    <ul class="product-filter-menu mt-5">
      @foreach($categories as $category)
        @if($category->id > 0)
          <li>
            <a href="#category-{{ $category->id }}">{{ $category->name }}</a>
          </li>
        @endif
      @endforeach
    </ul>

    <div class="row">
      @foreach($categories as $category)
        @if($category->id > 0)
          <div class="col-12 mb-5 mt-5">
            <h2 id="category-{{ $category->id }}">{{ $category->name }}</h2>
          </div>
          @foreach($products[$category->id] as $product)
            @include('partials.product_card', ['product' => $product])
          @endforeach
        @endif
      @endforeach
    </div>
  </section>

  <section class="container-fluid pb-5 mb-5">
    <div class="col-12 mb-5 mt-5">
      <div class="container">
        <h2>
          @if($page->social_id == 1)
            <span style="color: red;width: 40px;display: inline-block;position: relative;top: -2px;">
            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" role="img"
                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-youtube fa-w-18"><path
                  fill="currentColor"
                  d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"
                  class=""></path></svg>
          </span> Новинки видео
          @else
            <span style="color: red;width: 40px;display: inline-block;position: relative;top: -2px;">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                   id="Layer_1" x="0px" y="0px" viewBox="0 0 551.034 551.034"
                   style="enable-background:new 0 0 551.034 551.034;" xml:space="preserve">
  <g>

    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="275.517" y1="4.57" x2="275.517" y2="549.72"
                    gradientTransform="matrix(1 0 0 -1 0 554)">
      <stop offset="0" style="stop-color:#E09B3D"/>
      <stop offset="0.3" style="stop-color:#C74C4D"/>
      <stop offset="0.6" style="stop-color:#C21975"/>
      <stop offset="1" style="stop-color:#7024C4"/>
    </linearGradient>
    <path style="fill:url(#SVGID_1_);"
          d="M386.878,0H164.156C73.64,0,0,73.64,0,164.156v222.722   c0,90.516,73.64,164.156,164.156,164.156h222.722c90.516,0,164.156-73.64,164.156-164.156V164.156   C551.033,73.64,477.393,0,386.878,0z M495.6,386.878c0,60.045-48.677,108.722-108.722,108.722H164.156   c-60.045,0-108.722-48.677-108.722-108.722V164.156c0-60.046,48.677-108.722,108.722-108.722h222.722   c60.045,0,108.722,48.676,108.722,108.722L495.6,386.878L495.6,386.878z"/>

    <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="275.517" y1="4.57" x2="275.517" y2="549.72"
                    gradientTransform="matrix(1 0 0 -1 0 554)">
      <stop offset="0" style="stop-color:#E09B3D"/>
      <stop offset="0.3" style="stop-color:#C74C4D"/>
      <stop offset="0.6" style="stop-color:#C21975"/>
      <stop offset="1" style="stop-color:#7024C4"/>
    </linearGradient>
    <path style="fill:url(#SVGID_2_);"
          d="M275.517,133C196.933,133,133,196.933,133,275.516s63.933,142.517,142.517,142.517   S418.034,354.1,418.034,275.516S354.101,133,275.517,133z M275.517,362.6c-48.095,0-87.083-38.988-87.083-87.083   s38.989-87.083,87.083-87.083c48.095,0,87.083,38.988,87.083,87.083C362.6,323.611,323.611,362.6,275.517,362.6z"/>

    <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="418.31" y1="4.57" x2="418.31" y2="549.72"
                    gradientTransform="matrix(1 0 0 -1 0 554)">
      <stop offset="0" style="stop-color:#E09B3D"/>
      <stop offset="0.3" style="stop-color:#C74C4D"/>
      <stop offset="0.6" style="stop-color:#C21975"/>
      <stop offset="1" style="stop-color:#7024C4"/>
    </linearGradient>
    <circle style="fill:url(#SVGID_3_);" cx="418.31" cy="134.07" r="34.15"/>
  </g>
  </svg>
            </span> Последние посты
          @endif
        </h2>
      </div>
    </div>
    <div class="row d-flex align-items-center text-white" id="page-videos">

    </div>
  </section>

  <section class="container-fluid section-about">
    <div class="row row-full-width d-flex align-items-center text-white">
      <div class="col-12 col-lg-7 col-image bg-img order-1"
           style="background-image: url({{ $page->footer_image }});height: 600px;">
        <div class="overlay-gradient right "></div>
      </div>
      <div class="col col-text order-2 ">
        <div class="inner p-5">
          <h2 class="title">
            <span>{{ $page->footer_title }}</span>
          </h2>
          <div class="rte">
            <span>{!! $page->footer_text !!}</span>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('scripts')
  <script>
      $(function () {
          $.ajax({
              url: '{{ route('page.posts', request()->id) }}',
              method: 'GET',
              success: function (data) {
                  console.log(data);
                  $('#page-videos').html(data);
              },
              error: function (data) {
                  console.log(data);
              }
          });
      });
  </script>
@endpush

@push('styles')
  <style>
    .video-post {
      height: 200px;
      -webkit-background-size: cover;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      position: relative;
    }

    .video-post:hover .info {
      opacity: 1;
    }

    .video-post .info {
      transition: all .2s;
      opacity: 0;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      padding: 10px 20px;
      background-color: rgba(0, 0, 0, 0.6);
    }

    .video-post .info p {
      max-height: 70px;
      overflow: hidden;
    }

    .video-post .info h3 {
      max-height: 70px;
      overflow: hidden;
    }
  </style>
@endpush