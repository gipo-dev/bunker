@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Редактирование товара {{ $product->name }}
    @if(request()->id)
      <a href="{{ route('admin.product.delete', request()->id) }}" class="btn btn-danger float-right"
         title="Удалить товар"><span class="fa fa-trash"></span></a>
    @endif
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-box"></span> Редактирование</h6>
        </div>
        <div class="card-body">
          <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            {{-- Наименование--}}
            <div class="col-md-4 form-group">
              <label for="name">Наименование</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Кепка ранчо"
                     value="{{ $product->name }}" required>
            </div>

            {{-- Категория--}}
            <div class="col-md-4 form-group">
              <label for="category_id">Категория</label>
              <select class="form-control" id="category_id" name="category_id" required>
                <option>--Выберите категорию</option>
                @foreach($categories as $category)
                  <option value="{{ $category->id }}"
                          @if($product->category_id == $category->id)
                          selected
                      @endif
                  >{{ $category->name }}</option>
                @endforeach
              </select>
            </div>

            {{-- Описание--}}
            <div class="col-md-4 form-group">
              <label for="description">Описание</label>
              <textarea name="description" id="description" class="form-control" rows="7"
                        required>{{ $product->description }}</textarea>
            </div>

            {{-- Базовая цена--}}
            <div class="col-md-4 form-group">
              <label for="base_price">Комиссия</label>
              <input type="number" step="0.01" class="form-control" name="base_price" id="base_price"
                     value="{{ $product->base_price }}" placeholder="15.50" required>
            </div>

            {{-- Страница--}}
            <div class="col-md-4 form-group">
              <label for="user_page_id">Страница</label>
              <select class="form-control" id="user_page_id" name="user_page_id" required>
                <option>--Выберите страницу</option>
                @foreach($pages as $page)
                  <option value="{{ $page->id }}"
                          @if($product->user_page_id == $page->id)
                          selected
                      @endif
                  >{{ $page->footer_title }}</option>
                @endforeach
              </select>
            </div>

            {{-- Размеры --}}
            @if(count($sizes) > 0)
              <label class="col-md-4">Количество размеров</label>
              @foreach($sizes as $size)
                <div class="col-md-4 form-group">
                  <label class="sr-only" for="inlineFormInputGroup">{{ $size->name }}</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">{{ $size->name }}</div>
                    </div>
                    <input type="number" class="form-control" min="0" name="sizes[{{ $size->id }}]" required
                           @if(isset($product->sizes[$size->id]))
                           value="{{ $product->sizes[$size->id]->count }}"
                           @else
                           value="0"
                        @endif
                    >
                  </div>
                </div>
              @endforeach
            @elseif(request()->id)
              {{-- Количество--}}
              <div class="col-md-4 form-group">
                <label for="count">Количество</label>
                <input type="number" class="form-control" name="count" id="count"
                       value="{{ $product->count }}" placeholder="3" required>
              </div>
            @endif

            @if(request()->id)
              <div class="col-md-6 form-group">
                <label>Изображения</label>
                <div id="product-images" class="row">
                  @if($product->images)
                    @foreach($product->images as $image)
                      <div class="image d-inline-block col-md-5 mb-2">
                      <span class="btn btn-danger btn-sm position-absolute img-delete" data-id="{{ $image->id }}"><i
                            class="fa fa-trash"></i></span>
                        <img src="{{ $image->path }}" alt="..."
                             class="img-thumbnail float-left">
                      </div>
                    @endforeach
                    <div class="image d-inline-block col-md-5" id="new-image-insert">
                      <label class="img-thumbnail d-inline-block text-center" for="new-image"
                             style="width: 200px;height: 200px;line-height: 200px;font-size: 80px;">+</label>
                      <input type="file" accept="image/*" hidden id="new-image">
                    </div>
                  @endif
                </div>
              </div>
            @endif

            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Сохранить</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
    @if(request()->id)
    $('#new-image').change(function () {
        var data = new FormData();
        data.append('image', $(this)[0].files[0]);
        data.append('_token', '{{ csrf_token() }}');
        $.ajax({
            url: '{{ route('admin.product.addImage', request()->id) }}',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            success: function (data) {
                $('#new-image-insert').before($('<div class="image d-inline-block col-md-5 mb-2"><span class="btn btn-danger btn-sm position-absolute img-delete" data-id="' + data.id + '"><i class="fa fa-trash"></i></span><img src="' + data.path + '" alt="..." class="img-thumbnail float-left"></div>'));
            }
        });
    });
    $('.img-delete').click(function () {
        var id = $(this).data('id');
        var that = $(this);
        $.ajax({
            url: '{{ route('admin.product.deleteImage') }}/' + id,
            data: {
                _token: '{{ csrf_token() }}',
            },
            method: 'POST',
            success: function (data) {
                that.parents('.image')[0].remove();
            },
            error: function (data) {
                alert(data);
            }
        });
    });
    @endif
  </script>
@endpush

@push('styles')
  <style>
    .img-delete {
      margin-left: 10px;
      margin-top: 10px;
      z-index: 100;
    }
  </style>
@endpush