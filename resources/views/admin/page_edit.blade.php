@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Редактирование страницы {{ $page->footer_title }}
    @if(request()->id)
      <a href="{{ route('admin.page.delete', request()->id) }}" class="btn btn-danger float-right"
         title="Удалить пользователя"><span class="fa fa-trash"></span></a>
    @endif
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-globe"></span> Редактирование</h6>
        </div>
        <div class="card-body">
          <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            {{-- Владелец--}}
            <div class="col-md-4 form-group">
              <label for="user_id">Владелец</label>
              <select class="form-control" id="user_id" name="user_id" required>
                <option>--Выберите пользователя</option>
                @foreach($users as $user)
                  <option value="{{ $user->id }}"
                    @if($page->user_id == $user->id)
                      selected
                    @endif
                  >{{ $user->name }}</option>
                @endforeach
              </select>
            </div>

            {{-- Ссылка --}}
            <div class="col-md-4 form-group">
              <label for="slug">Ссылка</label>
              <input type="text" class="form-control" id="slug" name="slug" placeholder="demolition-ranch"
                     value="{{ $page->slug }}" required>
            </div>

            {{-- Соц. сеть --}}
            <div class="col-md-4 form-group">
              <label for="slug">Соц. сеть</label>
              <select name="social_id" class="custom-select" required>
                <option value="1" {{ $page->social_id == 1 ? 'selected' : '' }}>Youtube</option>
                <option value="2" {{ $page->social_id == 2 ? 'selected' : '' }}>Instagram</option>
              </select>
            </div>

            {{-- Канал --}}
            <div class="col-md-4 form-group">
              <label for="url">Ссылка соц. сеть</label>
              <span class="text-xs d-block">Например: https://www.youtube.com/channel/<b>UCdbcyBj6OO8lGMDQul1ansQ</b> <a
                    href="https://www.white-windows.ru/kak-uznat-id-kanala-na-youtube/" target="_blank">Помощь</a></span>
              <input type="text" class="form-control" id="url" name="link" placeholder="UCBvc7pmUp9wiZIFOXEp1sCg"
                     value="{{ $page->url }}" required>
            </div>

            {{-- Изображение в списке --}}
            <div class="col-12 form-group">
              <label for="small_image">Изображение в списке</label>
              @if($page->small_image && $page->small_image != '')
                <div>
                  <img src="{{ $page->small_image }}" alt="" class="img-thumbnail">
                  <div>
                    <label for="small_image" class="help-block"
                           style="text-decoration: underline;font-weight: 400;cursor: pointer;">Заменить фото</label>
                    <input type="file" id="small_image" name="small_image" accept="image/*" hidden class="hidden">
                  </div>
                </div>
              @else
                <div>
                  <input type="file" id="small_image" name="small_image" accept="image/*" required>
                </div>
              @endif
            </div>

            {{-- Главное изображение --}}
            <div class="col-12 form-group">
              <label for="header_image">Главное изображение</label>
              @if($page->header_image && $page->header_image != '')
                <div>
                  <img src="{{ $page->header_image }}" alt="" class="img-thumbnail">
                  <div>
                    <label for="header_image" class="help-block"
                           style="text-decoration: underline;font-weight: 400;cursor: pointer;">Заменить фото</label>
                    <input type="file" id="header_image" name="header_image" accept="image/*" hidden class="hidden">
                  </div>
                </div>
              @else
                <div>
                  <input type="file" id="header_image" name="header_image" accept="image/*" required>
                </div>
              @endif
            </div>

            {{-- Изображение описания --}}
            <div class="col-12 form-group">
              <h3 class="mb-3">Описание пользователя</h3>

              <label for="footer_image">Изображение описания</label>
              @if($page->footer_image && $page->footer_image != '')
                <div>
                  <img src="{{ $page->footer_image }}" alt="" class="img-thumbnail">
                  <div>
                    <label for="footer_image" class="help-block"
                           style="text-decoration: underline;font-weight: 400;cursor: pointer;">Заменить фото</label>
                    <input type="file" id="footer_image" name="footer_image" accept="image/*" hidden class="hidden">
                  </div>
                </div>
              @else
                <div>
                  <input type="file" id="footer_image" name="footer_image" accept="image/*" required>
                </div>
              @endif
            </div>

            {{-- Заголовок описания --}}
            <div class="col-md-4 form-group">
              <label for="footer_title">Заголовок описания</label>
              <input type="text" class="form-control" id="footer_title" name="footer_title"
                     placeholder="Разрушительное ранчо"
                     value="{{ $page->footer_title }}" maxlength="50" required>
            </div>

            {{-- Заголовок описания --}}
            <div class="col-md-6 form-group">
              <label for="footer_text">Описание</label>
              <div>
                <textarea class="form-control" id="footer_text" name="footer_text" placeholder="Описание" rows="8" maxlength="1000" required>{{ $page->footer_text }}</textarea>
              </div>
            </div>

            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Сохранить</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection