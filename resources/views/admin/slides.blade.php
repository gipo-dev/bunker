@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление слайдами
    <a href="{{ route('admin.slide.edit') }}" class="btn btn-primary float-right" title="Создать слайд"><span class="fa fa-plus"></span></a>
  </h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Слайды</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>Изображение</th>
            <th>Ссылка</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($slides as $slide)
            <tr>
              <td><img src="{{ $slide->path }}" alt="" style="max-width: 80px;max-height: 80px;"></td>
              <td>{{ $slide->link }}</td>
              <td>
                <a href="{{ route('admin.slide.edit', ['id' => $slide->id]) }}" class="btn btn-primary"><span
                      class="fa fa-edit"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $slides->links() }}
      </div>
    </div>
  </div>
@endsection