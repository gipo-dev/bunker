@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Редактирование пользователя {{ $user->name }}
    @if(request()->id)
      <a href="{{ route('admin.user.delete', request()->id) }}" class="btn btn-danger float-right"
         title="Удалить пользователя"><span class="fa fa-trash"></span></a>
    @endif
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Редактирование</h6>
        </div>
        <div class="card-body">
          <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            {{-- Имя--}}
            <div class="col-md-4 form-group">
              <label for="name">Имя</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Разрушительное ранчо"
                     value="{{ $user->name }}" required>
            </div>
            {{-- Почта --}}
            <div class="col-md-4 form-group">
              <label for="email">Почта</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="ranch@example.com"
                     value="{{ $user->email }}" required>
            </div>
            {{-- Пароль --}}
            <div class="col-md-4 form-group">
              <label for="slug">Пароль</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="********"
                     @if(request()->id == 'new')
                     required
                  @endif
              >
            </div>
            {{-- Аватар --}}
            <div class="col-12 form-group">
              <label for="avatar">Аватар</label>
              @if($user->avatar && $user->avatar != '')
                <div>
                  <img src="{{ $user->avatar }}" alt="" class="img-thumbnail">
                  <div>
                    <label for="avatar" class="help-block"
                           style="text-decoration: underline;font-weight: 400;cursor: pointer;">Заменить фото
                      паспорта</label>
                    <input type="file" id="avatar" name="avatar" accept="image/*" hidden class="hidden">
                  </div>
                </div>
              @else
                <div>
                  <input type="file" id="avatar" name="avatar" accept="image/*" required>
                </div>
              @endif
            </div>

            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Сохранить</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection