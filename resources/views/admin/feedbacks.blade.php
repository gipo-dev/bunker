@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Обратная связь</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Вопросы</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>id</th>
            <th>Имя</th>
            <th>Статус</th>
            <th>Почта</th>
            <th>Категория</th>
            <th>Тема</th>
            <th>Сообщение</th>
            <th>Дата добавления</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($messages as $message)
            <tr class="{{ $message->status_id == 1 ? 'border-left-success' : '' }}">
              <td>{{ $message->id }}</td>
              <td>{{ $message->name }}</td>
              <td>{{ $message->status->name }}</td>
              <td>{{ $message->email }}</td>
              <td>{{ $message->category ? $message->category->name : '' }}</td>
              <td>{{ $message->subject }}</td>
              <td>{{ $message->message }}</td>
              <td>{{ $message->created_at }}</td>
              <td>
                <a href="{{ route('admin.feedback.edit', ['id' => $message->id]) }}" class="btn btn-primary"><span
                      class="fa fa-eye"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $messages->links() }}
      </div>
    </div>
  </div>
@endsection