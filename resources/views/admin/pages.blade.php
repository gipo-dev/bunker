@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление страницами
    <a href="{{ route('admin.page.edit') }}" class="btn btn-primary float-right" title="Создать страницу"><span class="fa fa-plus"></span></a>
  </h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Страницы</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>Пользователь</th>
            <th>Название</th>
            <th>Ссылка</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($pages as $page)
            <tr>
              <td><a href="{{ route('admin.user', $page->user_id) }}">{{ $page->user ? $page->user->name : '' }}</a></td>
              <td>{{ $page->footer_title }}</td>
              <td>{{ $page->slug }}</td>
              <td>
                <a href="{{ route('admin.page.edit', ['id' => $page->id]) }}" class="btn btn-primary"><span
                      class="fa fa-edit"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $pages->links() }}
      </div>
    </div>
  </div>
@endsection