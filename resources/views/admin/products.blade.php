@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление товарами
    <a href="{{ route('admin.product.edit') }}" class="btn btn-primary float-right" title="Создать товар"><span class="fa fa-plus"></span></a>
  </h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Товары</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th></th>
            <th>Наименование</th>
            <th>Категория</th>
            <th>Страница</th>
            <th>Комиссия</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($products as $product)
            <tr>
              <td><img src="{{ isset($product->images->first()->path) ? $product->images->first()->path : '' }}" style="max-width: 75px;max-height: 75px;" alt=""></td>
              <td>{{ $product->name }}</td>
              <td>{{ $product->category->name }}</td>
              <td>{{ $product->page ? $product->page->footer_title : '' }}</td>
              <td>{{ $product->base_price }}</td>
              <td>
                <a href="{{ route('admin.product.edit', ['id' => $product->id]) }}" class="btn btn-primary"><span
                      class="fa fa-edit"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $products->links() }}
      </div>
    </div>
  </div>
@endsection