@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление пользователями
    <a href="{{ route('admin.user.edit', 'new') }}" class="btn btn-primary float-right" title="Создать пользователя"><span class="fa fa-plus"></span></a>
  </h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Пользователи</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>Имя пользователя</th>
            <th>Почтовый адрес</th>
            <th>Сайт</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($users as $user)
            <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td><a href="{{ $user->page ? route('admin.page.edit', $user->page->id) : '' }}">{{ $user->page ? $user->page->slug : '' }}</a></td>
              <td>
                <a href="{{ route('admin.user', ['id' => $user->id]) }}" class="btn btn-primary"><span
                      class="fa fa-eye"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $users->links() }}
      </div>
    </div>
  </div>
@endsection