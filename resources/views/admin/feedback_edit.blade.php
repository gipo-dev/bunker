@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Обратная связь</h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-globe"></span> Вопрос</h6>
        </div>
        <div class="card-body">
          <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            {{-- Имя --}}
            <div class="col-md-6 form-group">
              <label for="name">Имя</label>
              <b>{{ $feedback->name }}</b>
            </div>

            {{-- Категория --}}
            <div class="col-md-6 form-group">
              <label for="name">Категория</label>
              <b>{{ $feedback->category ? $feedback->category->name : '' }}</b>
            </div>

            {{-- Тема --}}
            <div class="col-md-6 form-group">
              <label for="name">Тема</label>
              <b>{{ $feedback->subject }}</b>
            </div>

            {{-- Сообщение --}}
            <div class="col-md-6 form-group">
              <label for="name">Сообщение</label>
              <b>{{ $feedback->message }}</b>
            </div>

            <div class="col-md-6 form-group">
              <label for="name">Дата добавления</label>
              <b>{{ $feedback->created_at }}</b>
            </div>

            {{-- Статус --}}
            <div class="col-md-4 form-group">
              <label for="status_id">Статус</label>
              <select class="form-control" id="status_id" name="status_id" required>
                @foreach($statuses as $status)
                  <option value="{{ $status->id }}"
                          @if($feedback->status_id == $status->id)
                          selected
                      @endif
                  >{{ $status->name }}</option>
                @endforeach
              </select>
            </div>

            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Сохранить</span>
            </button>

            {{-- Файлы --}}
            @if(json_decode($feedback->files))
              <div class="col-md-6 form-group mt-5">
                <h4>Файлы</h4>
                <div class="row">
                  @foreach(json_decode($feedback->files) as $file)
                    <div class="col-md-6 file-fs mb-3"><img src="{{ $file }}" alt="" class="img-thumbnail"></div>
                    {{--                <b>{{ $feedback->message }}</b>--}}
                  @endforeach
                </div>
              </div>
            @endif
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
      $('.file-fs').click(function () {
          $(this).toggleClass('col-md-6');
      });
  </script>
@endpush