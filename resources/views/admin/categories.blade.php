@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление категориями
    <a href="{{ route('admin.category.edit') }}" class="btn btn-primary float-right" title="Создать категорию"><span class="fa fa-plus"></span></a>
  </h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Категории</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>Название</th>
            <th>Ссылка</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($categories as $category)
            <tr>
              <td>{{ $category->name }}</td>
              <td>{{ $category->slug }}</td>
              <td>
                <a href="{{ route('admin.category.edit', ['id' => $category->id]) }}" class="btn btn-primary"><span
                      class="fa fa-edit"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $categories->links() }}
      </div>
    </div>
  </div>
@endsection