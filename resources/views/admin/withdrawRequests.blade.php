@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление запросами на вывод</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Запросы</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>№ транзакции</th>
            <th>Статус</th>
            <th>Пользователь</th>
            <th>Сумма</th>
            <th>Дата</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @if(count($requests) > 0)
            @foreach($requests as $req)
              <tr class="{{ $req->status_id == 1 ? 'border-left-success' : '' }}">
                <td>{{ $req->id }}</td>
                <td><a href="{{ route('admin.user', $req->user_id) }}">{{ $req->user->name }}</a></td>
                <td>{{ $req->status->name }}</td>
                <td>{{ $req->summ }}</td>
                <td>{{ $req->created_at }}</td>
                <td>
                  <a href="{{ route('admin.withdrawRequest.edit', $req->id) }}" class="btn btn-primary"><span
                        class="fa fa-eye"></span></a>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="6">
                <h4 class="text-center">Запросов на вывод пока что нет</h4>
              </td>
            </tr>
          @endif
          </tbody>
        </table>
        {{ $requests->links() }}
      </div>
    </div>
  </div>
@endsection