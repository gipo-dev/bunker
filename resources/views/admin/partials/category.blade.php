@foreach($categories as $category)
  <li class="list-group-item border-left-{{ $category->active ? '' : 'danger' }}">
    @if($category->sub)
      <span data-toggle="collapse" href="#childrens-{{ $category->id }}">
        {{ $category->name }}
        <span class="fa fa-chevron-down text-xs"></span>
      </span>
      <a href="{{ route('admin.category.edit', ['id' => $category->id]) }}" class="btn btn-primary btn-sm float-right">
        <span class="fa fa-pen"></span>
      </a>
      <ul class="list-group collapse mt-2" id="childrens-{{ $category->id }}">
        @include('admin.partials.category', [ 'categories' => $category->sub ])
        <div class="mt-2">
          <a href="{{ route('admin.category.edit', [ 'id' => null, 'parent_id' => $category->id ]) }}" class="btn btn-dark text-xs">Добавить сюда</a>
        </div>
      </ul>
    @else
      {{ $category->name }} <a href="{{ route('admin.category.edit', [ 'id' => null, 'parent_id' => $category->id ]) }}" class="btn btn-dark btn-sm ml-3">Добавить сюда</a>
      <a href="{{ route('admin.category.edit', ['id' => $category->id]) }}" class="btn btn-primary btn-sm float-right">
        <span class="fa fa-pen"></span>
      </a>
    @endif
  </li>
@endforeach