@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление пользователем {{ $user->name }}
    <a href="{{ route('admin.user.edit', request()->id) }}" class="btn btn-primary float-right"
       title="Редактировать пользователя"><span class="fa fa-edit"></span></a>
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Общая информация</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Имя</button>
              </td>
              <td>{{ $user->name }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Почта</button>
              </td>
              <td>{{ $user->email }}</span>
              </td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Страница</button>
              </td>
              <td>
                @if($user->page)
                  <a href="{{ route('page', $user->page->id) }}"
                     target="_blank">{{ $user->page->footer_title }}</a></span>
                @else
                  <a href="{{ route('admin.page.edit') }}">Создать</a>
                @endif
              </td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Баланс</button>
              </td>
              <td>{{ $user->wallet()->balance }}руб.
                <button class="btn btn-primary btn-sm text-xs" data-target="modal-successed"
                        onclick="$('#modal-successed').modal('show')">изменить
                </button>
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Зарегистрирован</button>
              </td>
              <td>{{ $user->created_at }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    {{-- история заказов --}}

    <div class="col-12 col-sm-12 col-xl-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-money-check"></span> История заказов
          </h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th>№</th>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Статус</th>
                <th>Создан</th>
              </tr>
              </thead>
              <tbody>
              @if($user->orders)
                @foreach($user->orders as $order)
                  <tr>
                    <td>
                      <a href="{{ route('admin.order.edit', $order->id) }}">{{ $order->id }}</a>
                    </td>
                    <td>{{ $order->name }}</td>
                    <td>{{ $order->phone }}</td>
                    <td>{{ $order->status->name }}</td>
                    <td>{{ $order->created_at }}</td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            @if($user->orders)
              {{ $user->orders->links() }}
            @endif
          </div>
        </div>
      </div>
    </div>

    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-search-dollar"></span> История транзакций
          </h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th>№ транзакции</th>
                <th>Тип транзакции (№ заказа)</th>
                <th>Сумма</th>
                <th>Дата</th>
              </tr>
              </thead>
              <tbody>
              @if(count($transactions) > 0)
                @foreach($transactions as $transaction)
                  <tr
                      class="{{ $transaction->transaction_data->summ > 0 ? "border-left-success" : "border-left-danger" }}">
                    <td>{{ $transaction->id }}</td>
                    <td>{{ $transaction->type->name }}
                      @if(isset($transaction->transaction_data->transaction->website_id))
                        (<a
                            href="{{ route('admin.website.edit', ['id' => $transaction->transaction_data->transaction->website_id]) }}">
                          вебсайт id{{ $transaction->transaction_data->transaction->website_id or '' }}
                        </a>)
                        (<a
                            href="{{ route('order', ['id' => $transaction->transaction_data->transaction->id]) }}">
                          заказ id{{ $transaction->transaction_data->transaction->id or '' }}
                        </a>)
                      @endif
                    </td>
                    <td>{{ $transaction->transaction_data->summ }}</td>
                    <td>{{ $transaction->created_at }}</td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="4">
                    <h4 class="text-center">История транзакций пользователя пока что пуста</h4>
                  </td>
                </tr>
              @endif
              </tbody>
            </table>
            @if($transactions)
              {{ $transactions->links() }}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('modals')
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-successed">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Подтвердите действие</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('admin.user.balance.add', \request()->id) }}" method="POST">
          {{ csrf_field() }}
          <div class="modal-body">
            <p><b>Внимание: </b>деньги будут автоматически переведены пользователю!
            </p>
            <input type="number" class="form-control" value="0" name="summ">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
            <button type="submit" class="btn btn-success">Отправить деньги</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endpush