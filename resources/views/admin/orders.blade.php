@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление заказами</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Заказы</h6>
    </div>
    <div class="card-body">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>№</th>
            <th>ФИО</th>
            <th>Телефон</th>
            <th>Почта</th>
            <th>Адрес</th>
            <th>Комментарий</th>
            <th>Статус</th>
            <th>Создан</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($orders as $order)
            <tr class="{{ $order->status->id == 1 ? 'border-left-success' : '' }}">
              <td>{{ $order->id }}</td>
              <td>{{ $order->name }}</td>
              <td>{{ $order->phone }}</td>
              <td>{{ $order->email }}</td>
              <td>{{ $order->address }}</td>
              <td>{{ $order->comment }}</td>
              <td>{{ $order->status->name }}</td>
              <td>{{ $order->created_at }}</td>
              <td>
                <a href="{{ route('admin.order.edit', $order->id) }}" class="btn btn-primary"><span
                      class="fa fa-eye"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $orders->links() }}
      </div>
    </div>
  </div>
@endsection