@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Редактирование страницы {{ $category->name }}
    @if(request()->id)
      <a href="{{ route('admin.category.delete', request()->id) }}" class="btn btn-danger float-right"
         title="Удалить пользователя"><span class="fa fa-trash"></span></a>
    @endif
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-globe"></span> Редактирование</h6>
        </div>
        <div class="card-body">
          <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            {{-- Название --}}
            <div class="col-md-4 form-group">
              <label for="name">Название</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Аксессуары"
                     value="{{ $category->name }}" required>
            </div>

            {{-- Ссылка --}}
            <div class="col-md-4 form-group">
              <label for="slug">Ссылка</label>
              <input type="text" class="form-control" id="slug" name="slug" placeholder="accessories"
                     value="{{ $category->slug }}" required>
            </div>

            {{-- Тип размера --}}
            <div class="col-md-4 form-group">
              <label for="size_type_id">Тип размера</label>
              <select class="form-control" id="size_type_id" name="size_type_id" required>
                @foreach($size_types as $type)
                  <option value="{{ $type->id }}"
                          @if($category->size_type_id == $type->id)
                          selected
                      @endif
                  >{{ $type->name }}</option>
                @endforeach
              </select>
            </div>

            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Сохранить</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection