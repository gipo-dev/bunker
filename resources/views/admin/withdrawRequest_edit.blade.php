@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Запрос на вывод средств №{{ $withdrawRequest->id }} <span
        class="btn btn-sm btn-{{ $withdrawRequest->status_id == 2 ? 'success' : 'primary' }}">{{ $withdrawRequest->status->name }}</span>
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-6 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Пользователь</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Имя пользователя
                </button>
              </td>
              <td><a href="{{ route('admin.user', $withdrawRequest->user->id) }}">{{ $withdrawRequest->user->id }}
                  .{{ $withdrawRequest->user->name }}</a></td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Сумма вывода
                </button>
              </td>
              <td>{{ $withdrawRequest->summ }}руб.</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Номер карты
                </button>
              </td>
              <td><input type="text" value="{{ $withdrawRequest->card_number }}" class="form-control form-control-sm">
              </td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Запрос создан
                </button>
              </td>
              <td>{{ $withdrawRequest->created_at }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-6 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-cog"></span> Управление</h6>
        </div>
        <div class="card-body">
          <form method="POST" id="settings">
            {{ csrf_field() }}
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="glyphicon glyphicon-cog"></span> Управление
              </div>
              <table class="table">
                <tbody>
                <tr>
                  <td style="width: 1%;">
                    <button class="btn btn-info">
                      Статус
                    </button>
                  </td>
                  <td>
                    <select name="status_id" class="form-control" id="input-request-status">
                      @foreach($statuses as $status)
                        <option
                            value="{{ $status->id }}" {{ $status->id == $withdrawRequest->status_id ? 'selected' : '' }}>{{ $status->name }}</option>
                      @endforeach
                    </select>
                  </td>
                </tr>
                <tr>
                  <td style="width: 1%;">
                    <button class="btn btn-info btn-sm text-xs">
                      Последнее обновления
                    </button>
                  </td>
                  <td>{{ $withdrawRequest->updated_at }}</td>
                </tr>
                <tr>
                  <td style="width: 1%;">
                    <button class="btn btn-info btn-sm text-xs">
                      Комментарий
                    </button>
                  </td>
                  <td><input type="text" name="comment" class="form-control form-control-sm" maxlength="500"
                             minlength="3" placeholder="Комментарий"></td>
                </tr>
                </tbody>
              </table>
              <button type="submit" class="btn btn-success btn-sm float-right" form="settings"><span
                    class="fa fa-save"></span> Обновить статус
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('modals')
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-successed">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <p><b>Внимание: </b>при активации этого статуса деньги будут автоматически списаны у пользователя!</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" id="modal-successed-ok">Принять и отправить
            деньги
          </button>
        </div>
      </div>
    </div>
  </div>
@endpush

@push('styles')
  <style>
    .table {
      margin-bottom: 0;
    }

    .table tr:first-child td {
      border-top: none;
    }
  </style>
@endpush

@push('scripts')
  <script>
      var last_selected = $('#input-order-status option:selected').val();
      $('#input-request-status').change(function (e) {
          if ($(this).val() == '2') {
              $(this).val(last_selected);
              $('#modal-successed').modal('show');
          } else {
              last_selected = $('#input-request-status option:selected').val();
          }
      });
      $('#modal-successed-ok').click(function () {
          $('#input-request-status').val('2');
      });
  </script>
@endpush