@extends('admin.layouts.app')

@section('title') Счет №{{ $order->id }} @endsection

@section('content')
  <div style="page-break-after: always;">
    <h1>Счет #{{ $order->id }}</h1>
    <table class="table table-bordered">
      <thead>
      <tr>
        <td colspan="2">Детали заказа</td>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td style="width: 50%;"><address>
            <strong>ООО БлогерСтор</strong><br>
          </address>
          <b>Телефон</b> +7 (8652) 93-66-93<br>
          <b>Адрес</b> Ставрополь ул.федеральная 16 оф6 <br>
          <b>Веб-сайт:</b> http://bloggerstor.ru</td>
        <td style="width: 50%;"><b>Дата добавления</b> {{ $order->created_at }}<br>
          <b>№ заказа:</b> {{ $order->id }}<br>
          <b>Способ оплаты</b> Безналичные расчет<br>
          <b>Способ доставки</b> Почта россии<br>
        </td>
      </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
      <tr>
        <td style="width: 50%;"><b>Адрес оплаты</b></td>
        <td style="width: 50%;"><b>Адрес доставки</b></td>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td><address>
            {{ $order->name }}<br>
            {{ $order->address }}<br>
          </address></td>
        <td><address>
            {{ $order->name }}<br>
            {{ $order->address }}<br>
          </address></td>
      </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
      <tr>
        <td><b>Товар</b></td>
        <td><b>Код товара</b></td>
        <td class="text-right"><b>Количество</b></td>
        <td class="text-right"><b>Цена за единицу товара</b></td>
        <td class="text-right"><b>Итого</b></td>
      </tr>
      </thead>
      <tbody>
      @php $total_price = 0 @endphp
      @foreach($order->products() as $product)
        <tr>
          @php $total_price += $product->price * $product->count @endphp
          <td>{{ $product->product->name }}</td>
          <td>{{ $product->product->slug }}</td>
          <td class="text-right">{{ $product->count ? $product->count : 0 }}</td>
          <td class="text-right">{{ $product->price }}р.</td>
          <td class="text-right">{{ $product->price * $product->count }}р.</td>
        </tr>
      @endforeach
      <tr>
        <td class="text-right" colspan="4"><b>Итого</b></td>
        <td class="text-right">{{ $total_price }}р.</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Доставка</b></td>
        <td class="text-right">300р.</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Всего</b></td>
        <td class="text-right">{{ $total_price + 300 }}р.</td>
      </tr>
      </tbody>
    </table>
  </div>
@endsection

@push('styles')
  <style>
    #accordionSidebar, .topbar {
      display: none;
    }
  </style>
@endpush

@push('scripts')
  <script>
      window.print();
  </script>
@endpush