@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Редактирование слайда {{ $slide->id }}
    @if(request()->id)
      <a href="{{ route('admin.slide.delete', request()->id) }}" class="btn btn-danger float-right"
         title="Удалить пользователя"><span class="fa fa-trash"></span></a>
    @endif
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-globe"></span> Редактирование</h6>
        </div>
        <div class="card-body">
          <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            {{-- Ссылка --}}
            <div class="col-md-4 form-group">
              <label for="link">Ссылка</label>
              <input type="text" class="form-control" id="link" name="link" placeholder="/blogger/1"
                     value="{{ $slide->link }}" required>
            </div>

            {{-- Изображение --}}
            <div class="col-12 form-group">
              <label for="small_image">Изображение</label>
              @if($slide->path && $slide->path != '')
                <div>
                  <img src="{{ $slide->path }}" alt="" class="img-thumbnail">
                  <div>
                    <label for="image" class="help-block"
                           style="text-decoration: underline;font-weight: 400;cursor: pointer;">Заменить фото</label>
                    <input type="file" id="image" name="image" accept="image/*" hidden class="hidden">
                  </div>
                </div>
              @else
                <div>
                  <input type="file" id="image" name="image" accept="image/*" required>
                </div>
              @endif
            </div>

            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Сохранить</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection