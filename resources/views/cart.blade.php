@extends('layouts.app')

@section('title') Корзина | BloggerStore @endsection

@section('content')
  <!-- cart section start -->
  <section class="cart-section spad">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="cart-table">
            <h3>Ваша корзина</h3>
            @if(count($products) > 0)
              <div class="cart-table-warp table-responsive">
                <table>
                  <thead>
                  <tr>
                    <th class="product-th">Товар</th>
                    <th class="quy-th">Размер</th>
                    <th class="quy-th">Количество</th>
                    <th class="total-th">Цена</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php
                    $total_price = 0;
                  @endphp
                  @foreach($products as $product)
                    <tr>
                      <td class="product-col">
                        <img
                            src="{{ $product->product->images->first() ? $product->product->images->first()->path : '' }}"
                            alt="">
                        <div class="pc-title">
                          <h4>{{ $product->product->name }}</h4>
                          <p>{{ $product->price }}руб.</p>
                          @php
                            $total_price += $product->price * $cart_products[$product->id]->count;
                          @endphp
                        </div>
                      </td>
                      <td class="size-col text-center">
                        <h4 class="p-0">{{ \App\Size::find($cart_products[$product->id]->size) ? \App\Size::find($cart_products[$product->id]->size)->name : '' }}</h4>
                      </td>
                      <td class="quy-col">
                        <div class="quantity">
                          <div class="pro-qty">
                            <input class="product-count" min="1" type="text"
                                   value="{{ $cart_products[$product->id]->count }}"
                                   data-product_id="{{ $product->id }}">
                          </div>
                        </div>
                      </td>
                      <td class="total-col product-price" data-product_price="{{ $product->price }}"><h4>{{ $product->price }}руб.</h4></td>
                      <td>
                        <a href="{{ route('cart.add', ['product_id' => $product->id, 'redir' => 1]) }}"
                           class="btn btn-dark btn-sm ml-3">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                  <tr class="text-right text-lg">
                    <td colspan="4">
                      <b>Доставка: 300руб.</b>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <div class="total-cost">
                <h6>Итого <span id="price-total">{{ $total_price + 300 }}руб.</span></h6>
              </div>
            @else
              <h5 class="mb-5">Ваша корзина пока что пуста</h5>
            @endif
          </div>
        </div>
        <div class="col-lg-4 card-right">
          @if(count($products) > 0)
            <a href="{{ route('checkout') }}" class="site-btn">Оформить заказ</a>
          @endif
          <a href="{{ route('products') }}" class="site-btn sb-dark">Продолжить покупки</a>
        </div>
      </div>
    </div>
  </section>
  <!-- cart section end -->
@endsection

@push('scripts')
  <script>
      $('.pro-qty').on('click', function () {
          var id = $(this).find('.product-count').data('product_id');
          var count = $(this).find('.product-count').val();
          $.ajax({
              url: '{{ route('cart.count') }}',
              data: {
                  product_id: id,
                  count: count,
              },
              success: function (data) {
                  var total_price = 0;
                  $('.product-price').each(function (i, el) {
                      total_price += $($('.product-count')[i]).val() * $(el).data('product_price');
                  });
                  $('#price-total').text((total_price + 300) + 'руб.');
              },
              error: function (data) {
                  console.log(data);
              }
          });
      });
  </script>
@endpush

@push('styles')
  <style>
    .cart-table .cart-table-warp {
      overflow: scroll !important;
    }
  </style>
@endpush