@extends('layouts.app')

@section('title') BloggerStore - интернет-магазин блоггерского мерча @endsection

@section('content')
  <!-- Hero section -->
  @if($slides->count() > 0)
    <section class="hero-section">
      <div class="hero-slider owl-carousel">
        @foreach($slides as $slide)
          <div class="hs-item set-bg" data-setbg="{{ $slide->path }}"><a href="{{ $slide->link }}"
                                                                         style="display: inline-block;width: 100%;height: 100%;"></a>
          </div>
        @endforeach
      </div>
      <div class="container">
        <div class="slide-num-holder" id="snh-1"></div>
      </div>
    </section>
  @endif
  <!-- Hero section end -->

  <!-- Product filter section -->
  <section class="product-filter-section pt-5">
    <div class="container mt-5">
      <div class="section-title">
        <h2>НОВЫЕ ПОСТУПЛЕНИЯ</h2>
      </div>
      <ul class="product-filter-menu">
        @foreach($categories as $category)
          <li><a href="{{ route('products', ['category' => $category->id]) }}">{{ $category->name }}</a></li>
        @endforeach
      </ul>
      <div class="row">
        @foreach($new_products as $product)
          @include('partials.product_card', ['product' => $product])
        @endforeach
      </div>
      @if($categories->first())
        <div class="text-center pt-5">
          <a href="{{ route('products') }}" class="site-btn sb-line sb-dark">БОЛЬШЕ
            ТОВАРОВ</a>
        </div>
      @endif
    </div>
  </section>
  <!-- Product filter section end -->
  <section class="product-filter-section pt-5">
    <div class="container mt-5">
      <div class="section-title">
        <h2>БЛОГЕРЫ</h2>
      </div>
      <div class="row">
        @foreach($pages as $page)
          @include('partials.user_page_card', ['page' => $page])
        @endforeach
      </div>
      <div class="text-center pt-5">
        <a href="{{ route('pages') }}" class="site-btn sb-line sb-dark">ВСЕ БЛОГЕРЫ</a>
      </div>
    </div>
  </section>
@endsection