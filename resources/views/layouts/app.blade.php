<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <title>@yield('title')</title>
  <meta charset="UTF-8">
  <meta name="description" content="Bloggerstor.ru - интернет магазин блогерского мерча.">
  <meta name="keywords" content="divisima, eCommerce, creative, html">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Favicon -->
  <link href="/img/favicon.ico" rel="shortcut icon"/>

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700&display=swap" rel="stylesheet">


  <!-- Stylesheets -->
  <link rel="stylesheet" href="/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="/css/font-awesome.min.css"/>
  <link rel="stylesheet" href="/css/flaticon.css"/>
  <link rel="stylesheet" href="/css/slicknav.min.css"/>
  <link rel="stylesheet" href="/css/jquery-ui.min.css"/>
  <link rel="stylesheet" href="/css/owl.carousel.min.css"/>
  <link rel="stylesheet" href="/css/animate.css"/>
  <link rel="stylesheet" href="/css/style.css?v=19233"/>
  @stack('styles')


  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
<!-- Page Preloder -->
<div id="preloder">
  <div class="loader"></div>
</div>

<!-- Header section -->
<header class="header-section" id="header-top">
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 px-0 text-center text-lg-left">
          <!-- logo -->
          <a href="/" class="site-logo">
            <img src="/img/logo.png" alt="">
          </a>
        </div>
        <div class="col-xl-7 col-lg-7 header-search d-sm-block">
          <form class="header-search-form" action="{{ route('search') }}">
            <input type="text" name="name" placeholder="Поиск на сайте ...." value="{{ request()->name }}">
            <button><i class="flaticon-search"></i></button>
          </form>
        </div>
        <!-- menu -->
        <ul class="col-xl-7 col-lg-7 main-menu" id="to-fixed-menu"></ul>
        <div class="col-xl-2 col-lg-2">
          <div class="user-panel">
            {{--<div class="up-item float-right">--}}
              {{--<i class="flaticon-profile"></i>--}}
              {{--<a href="{{ route('login') }}">Вход</a>--}}
            {{--</div>--}}
            <div class="up-item float-right mr-3">
              <div class="shopping-card">
                <i class="flaticon-bag"></i>
                <span id="cart-count">{{ count(\App\Http\Controllers\CartController::products()) }}</span>
              </div>
              <a href="{{ route('cart') }}">Корзина</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <nav class="main-navbar">
    <div class="container">
      <!-- menu -->
      <ul class="main-menu" id="t-menu">
        <li><a href="/">На главную</a></li>
        <li>
          <a href="{{ route('pages') }}" id="bloggers-menu">Блогеры</a>
        </li>
        <li>
          <a href="{{ route('page.subscribe') }}">Подписка на футболки</a>
        </li>
        <li>
          <a href="{{ route('feedback') }}">Обратная связь</a>
        </li>
        <li>
          <a href="{{ route('contacts') }}">Контакты</a>
        </li>
        <li>
          <a href="{{ route('about') }}">О нас</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="col-12 blogger-container">
      <div class="row blogger-wrap">
        @foreach(\App\Http\Controllers\WebsiteController::getPages() as $page)
          <div class="col-3 p-0 p-2 page-thumb blogger-item">
            <a href="{{ route('page', $page->slug) }}">
              <img src="{{ $page->small_image }}" alt="">
            </a>
          </div>
        @endforeach
        <div class="text-center pt-2 col-12">
          <a href="{{ route('pages') }}" class="site-btn sb-line sb-dark site-btn-white">ВСЕ БЛОГЕРЫ</a>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- Header section end -->

@yield('content')

<!-- Footer section -->
<section class="footer-section mt-5">
  <div class="container">
    {{--<div class="footer-logo text-center">--}}
      {{--<a href="/"><img src="/img/logo-light.png" alt=""></a>--}}
    {{--</div>--}}
    <div class="row">
      <div class="col-lg-3 col-sm-6">
        <div class="footer-widget about-widget">
          <h2>О нас</h2>
          <p>Donec vitae purus nunc. Morbi faucibus erat sit amet congue mattis. Nullam frin-gilla faucibus urna, id
            dapibus erat iaculis ut. Integer ac sem.</p>
          <img src="/img/cards.png" alt="">
        </div>
      </div>
      <div class="col-lg-3 col-sm-6">
        <div class="footer-widget about-widget">
          <h2>Полезные ссылки</h2>
          <ul>
            <li><a href="{{ route('questions') }}#how-to-buy">Как купить</a></li>
            <li><a href="{{ route('questions') }}#delivery">Доставка</a></li>
            <li><a href="{{ route('return') }}">Возврат</a></li>
            <li><a href="{{ route('feedback') }}">Обратная связь</a></li>
          </ul>
          <ul>
            <li><a href="{{ route('about') }}">О нас</a></li>
            <li><a href="{{ route('contacts') }}">Контакты</a></li>
            <li><a href="{{ route('job') }}">Вакансии</a></li>
            <li><a href="{{ route('login') }}">Вход</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Footer section end -->

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="" id="product-modal"
     aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content" id="modal-product-content">
{{--      @include('partials.product_modal', ['product' => (\App\PageProduct::with(['product', 'product.images', 'product.page', 'product.sizes'])->find(2))])--}}
    </div>
  </div>
</div>


<!--====== Javascripts & Jquery ======-->
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.slicknav.min.js?v=19233"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/jquery.nicescroll.min.js"></script>
<script src="/js/jquery.zoom.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/main.js?v=19234"></script>
@stack('scripts')

</body>
</html>