@extends('layouts.app')

@section('title') Блогеры | BloggerStore @endsection

@section('content')
  <section class="product-filter-section pt-5">
    <div class="container mt-5">
      <div class="section-title">
        <h2>БЛОГЕРЫ</h2>
      </div>
      <div class="row">
        @foreach($pages as $page)
          @include('partials.user_page_card', ['page' => $page])
        @endforeach
      </div>
    </div>
  </section>
@endsection