@extends('user.layouts.app')

@section('title') Управление товарами | BloggerStore @endsection

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление товарами</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Товары</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th></th>
            <th>Наименование</th>
            <th>Категория</th>
            <th>Остатки</th>
            <th>Комиссия</th>
            <th>Цена</th>
          </tr>
          </thead>
          <tbody>
          @foreach($products as $product)
            <tr>
              <td><img src="{{ isset($product->images->first()->path) ? $product->images->first()->path : '' }}"
                       style="max-width: 75px;max-height: 75px;" alt=""></td>
              <td>{{ $product->name }}</td>
              <td>{{ $product->category->name }}</td>
              <td>
                @if($product->sizes->count() > 0)
                  @foreach($product->sizes as $size)
                    {{ $size->size->name.': '.$size->count }},
                  @endforeach
                @else
                  {{ $product->count }}
                @endif
              </td>
              <td>{{ $product->base_price }}</td>
              <td>
                <input type="number" value="{{ $product->pageProduct ? $product->pageProduct->price : 0 }}"
                       min="{{  $product->base_price }}" data-product-id="{{ $product->id }}"
                       class="form-control form-control-sm price-input">
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $products->links() }}
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
      $('.price-input').change(function () {
          var el = $(this);
          var product_id = el.data('product-id');
          var price = el.val();
          $.ajax({
              url: '{{ route('user.products.changePrice') }}',
              data: {
                  _token: '{{ csrf_token() }}',
                  price: price,
                  product_id: product_id,
              },
              method: 'POST',
              success: function (data) {
                  // console.log(data);
              },
              error: function (data) {
                  console.log(data);
                  alert('Ошибка, цена не изменена');
              }
          });
      });
  </script>
@endpush