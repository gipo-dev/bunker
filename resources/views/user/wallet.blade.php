@extends('user.layouts.app')

@section('title') Баланс | BloggerStore @endsection

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление балансом</h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Текущий баланс</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user->wallet()->balance }}<i
                    class="fa fa-ruble-sign fa-xs"></i></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-ruble-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              @if($request == null)
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Вывод средств доступен</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#widthdraw-request">Вывести
                    средства
                  </button>
                </div>
              @else
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Вывод средств недоступен</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  Заявка в обработке
                </div>
              @endif
            </div>
            <div class="col-auto">
              <i class="fas fa-wallet fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">История запросов на вывод средств</h6>
    </div>
    <div class="card-body">
      <div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
              <th>№ транзакции</th>
              <th>Статус</th>
              <th>Сумма</th>
              <th>Дата</th>
            </tr>
            </thead>
            <tbody>
            @if(count($requests) > 0)
              @foreach($requests as $req)
                <tr>
                  <td>{{ $req->id }}</td>
                  <td>{{ $req->status->name }}</td>
                  <td>{{ $req->summ }}</td>
                  <td>{{ $req->created_at }}</td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="4">
                  <h4 class="text-center">История вывода пока что пуста</h4>
                </td>
              </tr>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">История транзакций</h6>
    </div>
    <div class="card-body">
      <div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
              <th>№ транзакции</th>
              <th>Тип транзакции (№ заказа)</th>
              <th>Сумма</th>
              <th>Дата</th>
            </tr>
            </thead>
            <tbody>
            @if(count($transactions) > 0)
              @foreach($transactions as $transaction)
                <tr
                    class="{{ $transaction->transaction_data->summ > 0 ? "border-left-success" : "border-left-danger" }}">
                  <td>{{ $transaction->id }}</td>
                  <td>{{ $transaction->type->name }}</td>
                  <td>{{ $transaction->transaction_data->summ > 0 ? '+'.$transaction->transaction_data->summ : $transaction->transaction_data->summ }}руб.</td>
                  <td>{{ $transaction->created_at }}</td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="4">
                  <h4 class="text-center">История ваших транзакций пока что пуста</h4>
                </td>
              </tr>
            @endif
            </tbody>
          </table>
          @if($transactions)
            {{ $transactions->links() }}
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection

@push('modals')
  <div class="modal fade" id="widthdraw-request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Создание запроса на вывод средств</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><b>Обратите внимание</b>, что одновреммено у вас может быть <b>не более одного запроса</b> на вывод
            средств.
            Создание нового запроса <b>будет недоступно</b>, пока модерация текущего запроса не будет закончена.</p>
          @if($user->wallet()->balance > 1)
            <form action="{{ route('user.wallet.withdrawRequests.add') }}" class="form-horizontal" id="widthdraw-request-form" method="POST">
              {{ csrf_field() }}
              <div class="form-group row">
                <label class="col-sm-5 col-form-label">Сумма для вывода</label>
                <div class="col-sm-6">
                  <p class="form-control-static">
                    <input type="number" value="{{ $user->wallet()->balance }}" min="1"
                           max="{{ $user->wallet()->balance }}"
                           class="form-control" name="summ" required>
                  </p>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-5 col-form-label">Номер карты получателя</label>
                <div class="col-sm-6">
                  <p class="form-control-static">
                    <input type="text"
                           class="form-control mask-card-number" name="card" required>
                  </p>
                </div>
              </div>
            </form>
          @else
            <h4>Минимальная сумма вывода составляет 1 рубль. На вашем счете недостаточно средств.</h4>
          @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
          @if($user->wallet()->balance > 1)
            <button type="submit" form="widthdraw-request-form" class="btn btn-primary btn-icon-split">
              <span class="icon text-white-50">
                <i class="far fa-clock"></i>
              </span>
              <span class="text">Отправить запрос</span>
            </button>
          @endif
        </div>
      </div>
    </div>
  </div>
@endpush

@push('scripts')
  <script>
      $('.mask-card-number').mask('9999 9999 9999 9999');
  </script>
@endpush
