@extends('user.layouts.app')

@section('title') Личный кабинет | BloggerStore @endsection

@section('content')
  <div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Текущий баланс</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                {{ $user->wallet()->balance }}<i class="fa fa-ruble-sign fa-xs"></i>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-ruble-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Новых заказов</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                {{ $user->orders->where('order_status_id', 1)->count() }}
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-truck-loading fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Всего заказов</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                {{ $user->orders->count() }}
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-boxes fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 mb-4">
      <div class="card shadow h-100">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Заказы</h6>
        </div>
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                <tr>
                  <th>№</th>
                  <th>ФИО</th>
                  <th>Товары</th>
                  <th>Статус</th>
                  <th>Создан</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                  <tr class="{{ $order->status->id == 4 ? 'border-left-success' : '' }}">
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->name }}</td>
                    <td class="text-xs">
                      @foreach($order->products() as $product)
                        {{ $product->product ? $product->product->name : '' }},
                      @endforeach
                    </td>
                    <td>{{ $order->status->name }}</td>
                    <td>{{ $order->created_at }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
              {{ $orders->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection