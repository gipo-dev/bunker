<form action='https://auth.robokassa.ru/Merchant/Index.aspx' method=POST id="payment-form">
  <input type=hidden name=MrchLogin value='{{ $data->mrh_login }}'>
  <input type=hidden name=OutSum value='{{ $data->out_summ }}'>
  <input type=hidden name=InvId value='{{ $data->inv_id }}'>
  <input type=hidden name=Desc value='{{ $data->inv_desc }}'>
  <input type="hidden" name="IncCurrLabel" value="{{ $data->incCurrLabel }}">
  <input type=hidden name=SignatureValue value='{{ $data->crc }}'>
  @isset($data->IsTest)
    <input type=hidden name=IsTest value='{{ $data->IsTest }}'>
  @endisset
  <input type=submit value=''>
</form>
<script>
  document.getElementById('payment-form').submit()
</script>