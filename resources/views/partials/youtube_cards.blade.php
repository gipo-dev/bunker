@if($type == 1)
  @foreach($posts->items as $video)
    <div class="col-md-3 p-1">
      <div class="video-post"
           style="background-image: url('{{ $video->snippet->thumbnails->high->url }}');">
        <a href="https://www.youtube.com/watch?v={{ $video->id->videoId }}" target="_blank">
          <div class="info">
            <h3>{{ $video->snippet->title }}</h3>
            <p>{{ $video->snippet->description }}</p>
            <b>{{ $video->snippet->channelTitle }}</b>
          </div>
        </a>
      </div>
    </div>
  @endforeach
@elseif($type == 2)
  @foreach($posts as $post)
    <div class="col-6 col-md-3 p-1">
      <div class="video-post"
           style="background-image: url('{{ $post->node->display_url }}');">
        <a href="https://www.instagram.com/p/{{ $post->node->shortcode }}" target="_blank">
          <div class="info">
            <p style="max-height: 135px;">{{ count($post->node->edge_media_to_caption->edges) > 0 ? $post->node->edge_media_to_caption->edges[0]->node->text : '' }}</p>
            <b>{{ '@'.$post->node->owner->username }}</b>
          </div>
        </a>
      </div>
    </div>
  @endforeach
@endif