<div class="col-lg-3 col-sm-6">
  <div class="product-item" data-product-id="{{ $product->id }}">
    <div class="pi-pic">
      <img src="{{ $product->product->images->first() ? $product->product->images->first()->path : '' }}" alt="">
      <div class="pi-links">
        @php
          $in_basket = isset(\App\Http\Controllers\CartController::products()[$product->id])
        @endphp
        <a class="add-card {{ $in_basket ? 'active' : '' }}" data-product-id="{{ $product->id }}">
          <i class="flaticon-bag"></i>
          <span class="text">{{ $in_basket ? 'В КОРЗИНЕ' : 'В КОРЗИНУ' }}</span>
        </a>
        <a class="wishlist-btn {{ in_array($product->id, \App\Http\Controllers\CartController::favoritesList()) ? 'active' : '' }}"
           data-product-id="{{ $product->id }}"><i class="flaticon-heart"></i></a>
      </div>
    </div>
    <div class="pi-text">
      <a href="{{ route('page', $product->product->user_page_id) }}"
         class="text-dark"><b>{{ $product->product->page->footer_title }}</b></a>
      <p>{{ $product->product->name }} </p>
      <b>{{ $product->price }}руб.</b>
    </div>
  </div>
</div>