<div class="col-md-4 p-0 page-thumb">
  <a href="{{ route('page', $page->slug) }}">
    <img src="{{ $page->small_image }}" alt="">
  </a>
</div>