<section class="product-section pt-5 position-relative">
  <button type="button" class="close py-2 px-3" data-dismiss="modal" aria-label="Close" style="position: absolute;right: 0;top: 0;">
    <span aria-hidden="true">&times;</span>
  </button>
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="product-pic-zoom">
          <img class="product-big-img"
               src="{{ $product->product->images->count() > 0 ? $product->product->images->first()->path : '' }}"
               alt="">
        </div>
        <div class="product-thumbs" tabindex="1" style="overflow: hidden; outline: none;">
          <div class="product-thumbs-track">
            @if($product->product->images)
              @foreach($product->product->images as $k => $image)
                <div class="pt {{ $k == 0 ? 'active' : '' }}" data-imgbigurl="{{ $image->path }}"><img
                      src="{{ $image->path }}"
                      alt=""></div>
              @endforeach
            @endif
          </div>
        </div>
      </div>
      <div class="col-lg-6 product-details">
        <form action="{{ route('cart.add') }}" id="to-basket-form">
          <input type="text" name="product_id" value="{{ $product->id }}" hidden>
          <h2 class="p-title mb-1">{{ $product->product->name }}</h2>
          <b>{{ $product->product->page->footer_title }}</b>
          <h3 class="p-price mt-3">{{ $product->price }}руб.</h3>
          <h4 class="p-stock">Доставка: <span>300руб.</span></h4>
          <div class="fw-size-choose">
            @if($product->product->sizes->count() > 0)
              <p>Размеры</p>
              @foreach($product->product->sizes as $k => $size)
                <div class="sc-item {{ $size->count < 1 ? 'disable' : '' }}">
                  <input type="radio" name="size" value="{{ $size->size->id }}"
                         id="size-{{ $k }}" {{ $size->count < 1 ? 'disabled' : '' }} required>
                  <label for="size-{{ $k }}">{{ $size->size->name }}</label>
                </div>
              @endforeach
            @endif
          </div>
          <div class="quantity">
            <p>Количество</p>
            <div class="pro-qty">
              <input type="text" name="count" value="1" max="20">
            </div>
          </div>
          <button type="submit"
                  class="site-btn to-basket {{ isset(\App\Http\Controllers\CartController::products()[$product->id]) ? 'sb-dark' : '' }}"
                  data-product-id="{{ $product->id }}"
                  id="to-basket-btn">{{ isset(\App\Http\Controllers\CartController::products()[$product->id]) ? 'В КОРЗИНЕ' : 'ДОБАВИТЬ В КОРЗИНУ' }}
          </button>
        </form>
        <div id="accordion" class="accordion-area">
          <div class="panel">
            <div class="panel-header" id="headingOne">
              <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true"
                      aria-controls="collapse1">описание
              </button>
            </div>
            <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="panel-body">
                <p>{{ $product->product->description }}</p>
              </div>
            </div>
          </div>
          <div class="panel">
            <div class="panel-header" id="headingThree">
              <button class="panel-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false"
                      aria-controls="collapse3">доставка и возврат
              </button>
            </div>
            <div id="collapse3" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="panel-body">
                <h4>Доставка осуществляется почтой России либо самовывозом, если вы находитесь в Ставрополе.</h4>
                <p>
                  Время доставки по территории России <span>до 14 рабочих дней</span>.
                </p>
                <p>
                  <span>Внимание!</span> Неправильно указанный номер телефона, неточный или неполный адрес могут
                  привести к
                  дополнительной задержке!
                </p>
                <p>
                  Пожалуйста, внимательно проверяйте ваши персональные данные при регистрации и оформлении
                  заказа. Конфиденциальность ваших регистрационных данных гарантируется.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
