@extends('layouts.app')

@section('title') Спасибо за заказ | BloggerStore @endsection

@section('content')
  <div class="page-top-info" style="margin-bottom: -3em;">
    <div class="container">
      <div class="site-pagination mb-3">
        <a href="/">Главная</a> /
        <a>Заказ оформлен</a>
      </div>
      <h4>Спасибо! Ваш заказ №{{ $order_id }} уже в обработке<br>Наш менеджер свяжется с вами в ближайшее время</h4>
      <div class="row text-center">
        {{--<div class="col-lg-4">--}}
          <a href="/" class="site-btn submit-order-btn mt-5 col-lg-3 col-6">Перейти на главную</a>
        {{--</div>--}}
      </div>
    </div>
  </div>
@endsection