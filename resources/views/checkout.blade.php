@extends('layouts.app')

@section('title') Оформление заказа | BloggerStore @endsection

@section('content')
  <div class="page-top-info">
    <div class="container">
      <h4>Оформление заказа</h4>
      <div class="site-pagination">
        <a href="/">Главная</a> /
        <a href="{{ route('cart') }}">Корзина</a>
      </div>
    </div>
  </div>

  <!-- checkout section  -->
  <section class="checkout-section spad">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 order-2 order-lg-1">
          <form action="" class="checkout-form" method="POST">
            {{ csrf_field() }}
            <div class="row address-inputs">
              <div class="col-md-12">
                <input type="text" name="name" maxlength="100" placeholder="Имя*" required>
              </div>
              <div class="col-md-6">
                <input type="text" name="phone" maxlength="20" placeholder="Номер телефона*" required>
              </div>
              <div class="col-md-6">
                <input type="email" name="email" maxlength="50" placeholder="E-mail*" required>
              </div>
              <div class="col-md-12">
                <input type="text" name="address" maxlength="500" placeholder="Адрес доставки*" required>
              </div>
              <div class="col-md-12">
                <input type="text" name="comment" maxlength="1000" placeholder="Комментарий к заказу">
              </div>
            </div>
            <button type="submit" class="site-btn submit-order-btn">Перейти к оплате</button>
          </form>
        </div>
        <div class="col-lg-4 order-1 order-lg-2">
          <div class="checkout-cart">
            <h3>Корзина</h3>
            <ul class="product-list">
              @php
                $total_price = 0;
              @endphp
              @foreach($products as $product)
                @php
                  $total_price += $product->price * $cart_products[$product->id]->count;
                @endphp
                <li>
                  <div class="pl-thumb"><img
                        src="{{ $product->product->images->first() ? $product->product->images->first()->path : '' }}"
                        alt=""></div>
                  <h6 class="p-0">{{ $product->product->name }}</h6>
                  <p>{{ $product->price }}руб.</p>
                  <p>{{ $cart_products[$product->id]->count }}шт.</p>
                </li>
              @endforeach
            </ul>
            <ul class="price-list">
              <li>Всего<span>{{ $total_price }}руб.</span></li>
              <li>Доставка<span>300руб.</span></li>
              <li class="total">Итого<span>{{ $total_price + 300 }}руб.</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- checkout section end -->
@endsection