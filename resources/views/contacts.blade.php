@extends('layouts.app')

@section('title') Контакты | BloggerStore @endsection

@section('content')
  <script src="https://api-maps.yandex.ru/2.1/?apikey=34d197d2-c30a-4d15-9075-086cd744236d&lang=ru_RU"
          type="text/javascript"></script>
  <!-- Page info -->
  <div class="page-top-info">
    <div class="container">
      <h4>Контакты</h4>
      <div class="site-pagination">
        <a href="/">Главная</a> /
        <a>Контакты</a>
      </div>
    </div>
  </div>
  <!-- Page info end -->


  <!-- Contact section -->
  <section class="contact-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 contact-info">
          <h3>Контактная информация</h3>
          <p>Адрес: г.Ставрополь ул. Федеральная, д. 16, офис 6</p>
          <p>Телефон: <a href="tel:88652936693">8 (8652) 93-66-93</a></p>
          <p>E-mail: <a href="mailto:info@blogger-store.ru">info@blogger-store.ru</a></p>
          <h4 class="mt-5 mb-5">Если вы хотите разработать свой мерч, либо разместить его на нашей площадке, просто
            напишите нам.</h4>
          <form action="{{ route('feedback.add') }}" method="POST" class="contact-form">
            @csrf
            <input type="text" name="category_id" value="4" hidden>
            <input type="text" name="name" placeholder="Ваше имя" required>
            <input type="text" name="email" placeholder="Ваш e-mail" required>
            <input type="text" name="subject" placeholder="Тема" required>
            <textarea name="message" placeholder="Расскажите подробнее о себе/о вашем предложении" maxlength="3000"
                      required></textarea>
            <button class="site-btn">Отправить</button>
          </form>
        </div>
      </div>
    </div>
    <div class="map" id="map">
    </div>
  </section>
  <!-- Contact section end -->

@endsection

@push('scripts')
  <script type="text/javascript">
      // Функция ymaps.ready() будет вызвана, когда
      // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
      ymaps.ready(init);

      function init() {
          // Создание карты.
          var myMap = new ymaps.Map("map", {
                  // Координаты центра карты.
                  // Порядок по умолчанию: «широта, долгота».
                  // Чтобы не определять координаты центра карты вручную,
                  // воспользуйтесь инструментом Определение координат.
                  center: [45.074235, 41.943320],
                  // Уровень масштабирования. Допустимые значения:
                  // от 0 (весь мир) до 19.
                  zoom: 17
              }),
              office = new ymaps.GeoObject({
                  // Описание геометрии.
                  geometry: {
                      type: "Point",
                      coordinates: [45.074235, 41.943320]
                  },
                  // Свойства.
                  properties: {
                      // Контент метки.
                      iconContent: 'Мы находимся здесь',
                      hintContent: 'г.Ставрополь ул. Федеральная, д. 16, офис 6',
                  }
              }, {
                  // Опции.
                  // Иконка метки будет растягиваться под размер ее содержимого.
                  preset: 'islands#redStretchyIcon',
                  // Метку можно перемещать.
                  draggable: false
              },
              // Placemark([45.074235, 41.943320], {}, {
              //
              // }
          );
          myMap.geoObjects
              .add(office);
      }
  </script>
@endpush