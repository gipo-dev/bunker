@extends('layouts.app')

@section('title') Вакансии | BloggerStore @endsection

@section('content')
  <!-- Page info -->
  <div class="page-top-info">
    <div class="container">
      <h4>Контакты</h4>
      <div class="site-pagination">
        <a href="/">Главная</a> /
        <a>Вакансии</a>
      </div>
    </div>
  </div>
  <!-- Page info end -->


  <!-- Contact section -->
  <section class="contact-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 contact-info">
          <h3>Вакансии</h3>
          <h4 class="mt-5 mb-2">Графический дизайнер</h4>
          <p>Если вы крутой графический дизайнер с кучей идей или просто хотите работать с нами ,то просто пришлите свое
            резюме через форму обратной связи.
            Так же вы можете присылать идеи принтов для блогеров уже размещенныйх на нашей платформе, если мы используем
            Вашу идею, то отправим Вам от <b>5000р.</b></p>
          <form action="{{ route('feedback.add') }}" method="POST" enctype="multipart/form-data" class="contact-form mt-5 pt-5">
            @csrf
            <input type="text" name="category_id" value="4" hidden>
            <input type="text" name="name" placeholder="Ваше имя" required>
            <input type="text" name="email" placeholder="Ваш e-mail" required>
            <input type="text" name="subject" placeholder="Тема" required>
            <textarea name="message" placeholder="Расскажите подробнее о себе/о вашем предложении" maxlength="3000"
                      required></textarea>
            <div class="custom-file mb-5">
              <input type="file" name="files[]" multiple class="custom-file-input" id="customFile" accept="image/*" lang="ru">
              <label class="custom-file-label" for="customFile">Прикрепите файлы</label>
            </div>
            <button class="site-btn">Отправить</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Contact section end -->

@endsection

@push('scripts')
  <script>

  </script>
@endpush