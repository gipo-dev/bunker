@extends('layouts.app')

@section('title') Обратная связь | BloggerStore @endsection

@section('content')
  <!-- Page info -->
  <div class="page-top-info">
    <div class="container">
      <h4>Обратная связь</h4>
      <div class="site-pagination">
        <a href="/">Главная</a> /
        <a>Обратная связь</a>
      </div>
    </div>
  </div>
  <!-- Page info end -->


  <!-- Contact section -->
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <section class="contact-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 contact-info">
          <h3>Задайте ваш вопрос</h3>
          <form action="{{ route('feedback.add') }}" method="POST" class="contact-form">
            @csrf
            <input type="text" name="name" placeholder="Ваше имя" required>
            <input type="text" name="email" placeholder="Ваш e-mail" required>
            <select name="category_id" class="custom-select" required>
              <option disabled selected>--Выберите категорию</option>
              @foreach($categories as $category)
                <option value="{{ $category->id }}" name="category_id">{{ $category->name }}</option>
              @endforeach
            </select>
            <input name="subject" type="text" placeholder="Тема" required>
            <textarea name="message" placeholder="Сообщение" minlength="10" required></textarea>
            <button type="submit" class="site-btn">Отправить</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Contact section end -->
@endsection

@push('scripts')
  <script>
      $(function () {
        {{--$.ajax({--}}
        {{--url: '{{ route('page.posts', request()->id) }}',--}}
        {{--method: 'GET',--}}
        {{--success: function (data) {--}}
        {{--console.log(data);--}}
        {{--$('#page-videos').html(data);--}}
        {{--},--}}
        {{--error: function (data) {--}}
        {{--console.log(data);--}}
        {{--}--}}
        {{--});--}}
      });
  </script>
@endpush

@push('styles')
  <style>

  </style>
@endpush