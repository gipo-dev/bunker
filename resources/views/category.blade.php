@extends('layouts.app')

@section('title') Категории | BloggerStore @endsection

@section('content')
  <!-- Category section -->
  <section class="category-section spad">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 order-2 order-lg-1">
          <form action="">
            <div class="filter-widget">
              <h2 class="fw-title">Категории</h2>
              <ul class="category-menu">
                @foreach($categories as $category)
                  @if($category->id > 0)
                    <li>
                      <a href="{{ route('products', ['category' => $category->id, 'name' => request()->name]) }}"
                         class="{{ request()->category == $category->id ? 'active' : '' }}">{{ $category->name }}</a>
                    </li>
                  @endif
                @endforeach
              </ul>
            </div>
            <div class="filter-widget mb-0">
              <h2 class="fw-title">Фильтр</h2>
              <div class="price-range-wrap">
                <h4>Цена</h4>
                <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                     data-min="{{ $minMaxPrice['min'] }}" data-max="{{ $minMaxPrice['max'] }}">
                  <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div>
                  <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;">
								</span>
                  <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;">
								</span>
                </div>
                <div class="range-slider">
                  <div class="price-input">
                    <input type="text" id="minamount" name="price[min]">
                    <input type="text" id="maxamount" name="price[max]">
                  </div>
                </div>
              </div>
            </div>
            <div class="filter-widget mb-3">
              <h2 class="fw-title">Блогер</h2>
              <ul class="category-menu">
                @foreach($pages as $page)
                  <li><a href="{{ route('page', $page->id) }}">{{ $page->footer_title }}</a></li>
                @endforeach
              </ul>
            </div>

            <div class="text-center w-100 pt-3">
              <button type="submit" class="site-btn sb-line sb-dark">Фильтровать</button>
            </div>
            <div class="text-center w-100 pt-3">
              <a href="{{ route('products') }}" class="site-btn sb-line sb-dark">Сбросить</a>
            </div>
          </form>
        </div>

        <div class="col-lg-9  order-1 order-lg-2 mb-5 mb-lg-0">
          @if(request()->name)
            <h2 class="mb-5">Результаты поиска по запросу: {{ request()->name }}</h2>
          @endif
          <div class="row">
            @if($products->count() > 0)
              @foreach($products as $product)
                @include('partials.product_card', ['product' => $product])
              @endforeach
              {{ $products->links() }}
            @else
              <h3>Товары не найдены</h3>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Category section end -->
@endsection