@extends('layouts.app')

@section('content')
  <section class="content-section video-section">
    <div class="pattern-overlay">
      <a id="bgndVideo" class="player"
         data-property="{videoURL:'https://www.youtube.com/watch?v=fdJc1_IBKJA',containment:'.video-section', quality:'large', autoPlay:true, mute:true, opacity:1}">bg</a>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2>Подпишитесь на любимого блогера</h2>
            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aut blanditiis cumque ea fugiat molestias
              nam, quas suscipit temporibus vero.</h3>
          </div>
        </div>
      </div>
    </div>
  </section>
  <form action="" method="POST" id="sub-form">
    <section class="top-letest-product-section">
      <div class="container">
        <div class="section-title text-left">
          <h2>ВЫБЕРИТЕ БЛОГЕРА</h2>
        </div>
        <div class="blogger-slider owl-carousel">
          @foreach($pages as $page)
            <div class="item blogger-select">
              <input type="radio" name="page_id" value="{{ $page->id }}" id="page-{{ $page->id }}" required>
              <label for="page-{{ $page->id }}">
                <div class="blogger-select-image" style="background-image: url('{{ $page->small_image }}')"></div>
              </label>
            </div>
          @endforeach
        </div>
      </div>
    </section>
    <section class="top-letest-product-section pt-0">
      <div class="container">
        <div class="section-title text-left">
          <h2>ВЫБЕРИТЕ ТИП ПОДПИСКИ</h2>
        </div>
        <div class="row" id="subscribes">
        </div>
        {{--<div class="mt-5">--}}
          {{--<input type="submit" class="site-btn" value="Оформить подписку" id="submit-subscribe">--}}
        {{--</div>--}}
      </div>
    </section>
  </form>
@endsection

@push('styles')
  <style>
    .blogger-select {
      width: 375px;
      height: 200px;
      display: inline-block;
    }

    .blogger-select .blogger-select-image {
      width: 375px;
      height: 200px;
      background-position: center;
      -webkit-background-size: cover;
      background-size: cover;
    }

    .blogger-select label {
      border: 3px solid rgba(0, 0, 0, 0);
      position: relative;
    }

    .blogger-select input[type=radio] {
      position: absolute;
      top: 0;
      opacity: 0;
    }

    .blogger-select input[type=radio]:checked + label {
      border: 3px solid #f51167;
    }

    .blogger-select input[type=radio]:checked + label::after {
      content: 'ВЫБРАН';
      position: absolute;
      bottom: 10px;
      right: 10px;
      background-color: #f51167;
      color: #fff;
      font-weight: 900;
      font-size: 12px;
      padding: 3px 7px;
      border-radius: 10px;
    }

    .disable-controlls {
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
    }

    .video-section .pattern-overlay {
      background-color: rgba(71, 71, 71, 0.59);
      padding: 110px 0 32px;
      min-height: 496px;
      /* Incase of overlay problems just increase the min-height*/
    }

    .video-section h2, .video-section h3 {
      text-align: center;
      color: #fff;
    }

    .video-section h2 {
      /*font-size:110px;*/
      /*font-weight:bold;*/
      text-transform: uppercase;
      margin: 40px auto 0px;
      text-shadow: 1px 1px 1px #000;
      -webkit-text-shadow: 1px 1px 1px #000;
      -moz-text-shadow: 1px 1px 1px #000;
    }

    .video-section h3 {
      font-size: 25px;
      font-weight: lighter;
      margin: 0px auto 15px;
    }

    .video-section .buttonBar {
      display: none;
    }

    .player {
      font-size: 1px;
    }
  </style>
@endpush

@push('scripts')
  <script src="/js/jquery.mb.YTPlayer.min.js"></script>
  <script>
      $('.blogger-select').click(function (e) {
          $.ajax({
              url: '{{ route('page.subscribes') }}',
              data: $('#sub-form').serializeArray(),
              success: function (data) {
                  console.log(data);
                  $('#subscribes').empty();
                  $('#subscribes').append(data);
              },
              error: function (data) {
                  console.log(data);
              }
          });
      });
      $('#subscribes').on('click', '.subscribe-item', function () {
          getModalProduct($(this).data('product-id'));
      });
      $(document).ready(function () {
          $(".player").mb_YTPlayer();
      });
      $('.blogger-slider').owlCarousel({
          margin: 10,
      });
  </script>
@endpush