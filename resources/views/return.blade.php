@extends('layouts.app')

@section('title') Возврат товара | BloggerStore @endsection

@section('content')
  <!-- Page info -->
  <div class="page-top-info">
    <div class="container">
      <h4>Возврат товара</h4>
      <div class="site-pagination">
        <a href="/">Главная</a> /
        <a>Возврат товара</a>
      </div>
    </div>
  </div>
  <!-- Page info end -->


  <!-- Contact section -->
  <section class="contact-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 contact-info">
          <h3>Возврат товара</h3>
          <form action="{{ route('feedback.add') }}" method="POST" enctype="multipart/form-data" class="contact-form">
            @csrf
            <input type="text" name="category_id" value="1" hidden>
            <input type="text" name="name" placeholder="Ваше имя" required>
            <input type="text" name="email" placeholder="Ваш e-mail или телефон" required>
            <input type="text" name="subject" placeholder="Тема" value="Возврат товара" hidden required>
            <textarea name="message" placeholder="Расскажите подробнее о том, почему вы хотите вернуть покупку." maxlength="3000"
                      required></textarea>
            <div class="custom-file mb-5">
              <input type="file" name="files[]" multiple class="custom-file-input" id="customFile" accept="image/*" lang="ru">
              <label class="custom-file-label" for="customFile">Прикрепите фотографии</label>
            </div>
            <button class="site-btn">Отправить</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Contact section end -->

@endsection

@push('scripts')
  <script>

  </script>
@endpush