<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawRequest extends Model
{
    public function status() {
        return $this->belongsTo('App\WithdrawRequestStatus', 'status_id');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getStateAttribute() {
        return decrypt($this->attributes['state']);
    }

    public function setStateAttribute($val) {
        $this->attributes['state'] = encrypt($val);
    }
}

class WithdrawRequestStatus extends Model
{

}