<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    private $wallet = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function page() {
        return $this->hasOne('App\UserPage');
    }

    public function transactions() {
        return $this->hasMany('App\Transaction')->with('type');
    }

    public function wallet()
    {
        if (!$this->wallet) {
            $this->wallet = new UserWallet($this);
        }
        return $this->wallet;
    }

    public function orders() {
        return $this->belongsToMany('App\Order');
    }

    public function withdrawRequests() {
        return $this->hasMany('App\WithdrawRequest')->with('status');
    }
}