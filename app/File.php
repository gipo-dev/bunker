<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class File extends Model
{

    public static function upload($file, $addingPath = '')
    {
        $filename = time() . $file->getClientOriginalName();
        Storage::disk('public')->putFileAs(
            'uploads' . $addingPath,
            $file,
            $filename
        );
        $path = '/storage/uploads'.$addingPath.'/'.$filename;
        $file = new File();
        if(Auth::user())
            $file->user_id = Auth::user()->id;
        else
            $file->user_id = 1;
        $file->path = $path;
        $file->save();
        return $file;
    }
}
