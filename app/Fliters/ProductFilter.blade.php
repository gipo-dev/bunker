<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Model;

class ProductFilter extends QueryFilter
{
    public function name($value) {
        $this->builder->where('name', 'like', "%$value%");
    }

}