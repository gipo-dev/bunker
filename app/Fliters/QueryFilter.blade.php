<?php

namespace App\Filters;

abstract class QueryFilter
{
    protected $builder;

    protected $request;

    public function __construct($builder, $request)
    {
        $this->builder = $builder;
        $this->request = $request;
    }

    public function apply() {
        foreach ($this->filters() as $filter => $value) {
            if($value == '')
                continue;
            if(method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    public function filters() {
        return $this->request->all();
    }

    public function id($value) {
        $this->builder->where('id', $value);
    }

    public function sort($value) {
        $this->builder->orderBy($value, $this->request->increase == 1 ? 'asc' : 'desc');
    }
}
