<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Model;

class PageProductFilter extends QueryFilter
{
    public function category($value)
    {
        $this->builder->whereHas('product', function ($q) {
            $q->where('category_id', '=', request()->category);
        });
    }

    public function name($value)
    {
        $this->builder->whereHas('product', function ($q) {
            $q->where('name', 'like', '%' . \request()->name . '%');
        });
    }

    public function price($value)
    {
        $min = str_replace('руб.', '', $value['min']);
        $max = str_replace('руб.', '', $value['max']);
        if ($min != '' && $max != '') {
            $this->builder->where('price', '>=', $min)->where('price', '<=', $max);
        }
    }

}