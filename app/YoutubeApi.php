<?php

namespace App;

use GuzzleHttp\Client;

class YoutubeApi
{
    public $apiKey = "AIzaSyDW3uNb3wYhPXqDPj_qe386V_-AO74Z1AA";

    public function getChannelLastVideos($channel)
    {
        if (!$channel || !$this->apiKey)
            return;
        return $this->sendRequest("https://www.googleapis.com/youtube/v3/search?key=$this->apiKey&channelId=$channel&part=snippet,id&order=date&maxResults=8");
    }

    public function sendRequest($url, $data = [], $method = 'GET')
    {
        $client = new Client();
        try {
            $resp = $client->request($method, $url, [
                'form_params' => $data,
            ])->getBody()->getContents();
        } catch (\Exception $ex) {
            $resp = '';
        }

        return json_decode($resp);
    }
}
