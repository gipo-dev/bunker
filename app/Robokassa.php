<?php

namespace App;

class Robokassa
{
    // регистрационная информация (логин, пароль #1)
    // registration info (login, password #1)
    public $mrh_login;
    public $mrh_pass1;

    // номер заказа
    // number of order
    public $inv_id = null;

    // описание заказа
    // order description
    public $inv_desc = "Оплата заказа на сайте Blogger-store.ru";

    // сумма заказа
    // sum of order
    public $out_summ = null;

    public $incCurrLabel = "BankCard";

    // тип товара
    // code of goods
    public $shp_item = 1;

    // язык
    // language
    public $culture = "ru";

    // кодировка
    // encoding
    private $encoding = "utf-8";

    public function __construct($inv_id, $inv_desc, $out_summ)
    {
        $this->mrh_login = env('ROBOKASSA_LOGIN');
        if(env('ROBOKASSA_TEST_MODE')) {
            $this->mrh_pass1 = env('ROBOKASSA_PASSWORD_TEST_1');
            $this->IsTest = 1;
        }
        else
            $this->mrh_pass1 = env('ROBOKASSA_PASSWORD_1');
        $this->inv_id = $inv_id;
        $this->out_summ = $out_summ;
        $this->inv_desc = $inv_desc;
    }

    public function payment() {
        // формирование подписи
        $this->crc = md5("$this->mrh_login:$this->out_summ:$this->inv_id:$this->mrh_pass1");
        return view('robokassa', ['data' => $this]);
    }
}
