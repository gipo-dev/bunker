<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['name', 'email', 'subject', 'message', 'category_id', 'status_id'];

    public function status() {
        return $this->belongsTo('App\FeedbackStatus', 'status_id');
    }

    public function category() {
        return $this->belongsTo('App\FeedbackCategory', 'category_id');
    }
}
class FeedbackStatus extends Model
{
    //
}
class FeedbackCategory extends Model
{
    //
}