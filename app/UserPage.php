<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPage extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function products() {
        return $this->belongsToMany('App\Product')->whereNotIn('category_id', [-1, -2, -3]);
    }

    public function pageProducts() {
        return$this->hasMany('App\PageProduct', 'page_product_id');
    }
}
