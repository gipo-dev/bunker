<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PageProduct extends Model
{
    protected $fillable = ['page_product_id', 'product_id', 'price'];

    public $timestamps = false;

    public $incrementing = false;

    public function product() {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
