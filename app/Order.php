<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name', 'phone', 'email', 'address', 'comment', 'products', 'state', 'order_status_id'];

    public function getProductsAttribute()
    {
        return (array)json_decode($this->attributes['products']);
    }

    public function status()
    {
        return $this->belongsTo('App\OrderStatus', 'order_status_id');
    }

    public function products()
    {
        $products = PageProduct::with('product')->find(array_keys($this->products));
        $sizes = Size::all()->keyBy('id');
        foreach ($products as $product) {
            $product->count = $this->products[$product->id]->count;
            $product->size = $sizes[$this->products[$product->id]->size]->name;
        }
        return $products;
    }

    public function history() {
        return $this->hasMany('App\OrderHistory')->orderBy('id', 'desc')->with('status');
    }

    public function getStateAttribute() {
        return decrypt($this->attributes['state']);
    }

    public function setStateAttribute($val) {
        $this->attributes['state'] = encrypt($val);
    }

    public function user() {
        return $this->belongsToMany('App\User');
    }
}

class OrderStatus extends Model
{

}

class OrderHistory extends Model
{
    public function status() {
        return $this->belongsTo('App\OrderStatus', 'order_status_id');
    }
}