<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getLMenus() {
        $items = [
            [
                'name' => 'Главная',
                'link' => route('user.home'),
                'icon' => 'home',
            ],
            [
                'name' => 'Товары',
                'link' => route('user.products'),
                'icon' => 'box',
            ],
            [
                'name' => 'Баланс',
                'link' => route('user.wallet'),
                'icon' => 'wallet',
            ],
        ];
        return $items;
    }

    public function index()
    {
        $user = Auth::user();
        $orders = $user->orders()->orderBy('id', 'desc')->paginate(10);
//        dd($orders->first()->products());
        return view('user.home', ['user' => $user, 'orders' => $orders]);
    }
}
