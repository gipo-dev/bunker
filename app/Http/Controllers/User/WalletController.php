<?php

namespace App\Http\Controllers\User;

use App\Telegram\TelegramBot;
use App\WithdrawRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    public function index(Request $request) {
        $user = Auth::user();
        $transactions = $user->transactions()->orderBy('id', 'desc')->paginate(10, ['*'], 'transactions');
        $request = $user->withdrawRequests()->where('status_id', 1)->first();
        $requests = $user->withdrawRequests()->orderBy('id', 'desc')->paginate(20, ['*'], 'requests');
        return view('user.wallet', ['user' => $user, 'transactions' => $transactions, 'request' => $request, 'requests' => $requests]);
    }

    public function makeWithdrawRequest(Request $request) {
        $withdrawRequest = new WithdrawRequest();
        $withdrawRequest->user_id = Auth::user()->id;
        $withdrawRequest->summ = $request->summ;
        $withdrawRequest->card_number = $request->card;
        $withdrawRequest->comment = '';
        $withdrawRequest->status_id = 1;
        $withdrawRequest->state = 0;
        $withdrawRequest->save();
        TelegramBot::sendMessage("Пользователь ". Auth::user()->name ." заказал вывод средств");
        return redirect(route('user.wallet'))->with('status', 'Запрос на вывод отправлен на модерацию');
    }
}
