<?php

namespace App\Http\Controllers\User;

use App\Product;
use App\PageProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function list() {
        $products = Product::where('user_page_id', Auth::user()->page->id)->orderBy('id', 'desc')->with(['pageProduct', 'sizes'])->paginate(20);
        return view('user.products', ['products' => $products]);
    }

    public function changePrice(Request $request) {
        PageProduct::updateOrCreate([
            'page_product_id' => Auth::user()->page->id,
            'product_id' => $request->product_id,
        ], [
            'price' => $request->price,
        ]);
    }
}
