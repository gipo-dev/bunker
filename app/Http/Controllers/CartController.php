<?php

namespace App\Http\Controllers;

use App\Order;
use App\PageProduct;
use App\Robokassa;
use App\Size;
use App\Telegram\TelegramBot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    private static $products;
    private static $favorites;

    public function add(Request $request)
    {
        $products = (array)json_decode($request->cookie('cart'));
        if (!$products)
            $products = [];
        if (isset($products[$request->product_id])) {
            unset($products[$request->product_id]);
        } else {
            $products[$request->product_id] = [
                'size' => $request->size,
                'count' => $request->count,
            ];
        }
        if ($request->redir)
            return redirect(route('cart'))->withCookie('cart', json_encode((array)$products), 10080);
        else
            return response(count($products))->withCookie('cart', json_encode((array)$products), 10080);
    }

    public function count(Request $request)
    {
        $products = (array)json_decode($request->cookie('cart'));
        if (!$products)
            $products = [];
        if (isset($products[$request->product_id])) {
            $products[$request->product_id]->count = $request->count;
//            return response()->json($products);
            return response('ok')->withCookie('cart', json_encode($products), 10080);
        }
    }

    public function favorites(Request $request)
    {
        $products = (array)json_decode($request->cookie('favorites'));
        if (!$products)
            $products = [];
        if (($key = array_search($request->product_id, $products)) !== false) {
            unset($products[$key]);
        } else
            $products[] = $request->product_id;
        return response(count($products))->withCookie('favorites', json_encode((array)$products), 10080);
    }

    public static function products()
    {
        if (!self::$products)
            self::$products = json_decode(\request()->cookie('cart'));
        return self::$products ? (array)self::$products : [];
    }

    public static function favoritesList()
    {
        if (!self::$favorites)
            self::$favorites = json_decode(\request()->cookie('favorites'));
        return self::$favorites ? (array)self::$favorites : [];
    }

    public function index()
    {
        $cart_products = self::products();
        $products = PageProduct::with(['product', 'product.images', 'product.sizes'])->find(array_keys($cart_products));
        return view('cart', ['cart_products' => $cart_products, 'products' => $products]);
    }

    public function checkout(Request $request)
    {
        if ($request->isMethod('POST')) {
            if (count($products = self::products()) < 1)
                return redirect(route('cart'));
            $data = array_merge($request->except('_token'), [
                'state' => encrypt(0),
                'order_status_id' => 8,
                'products' => json_encode($products),
            ]);
            $order = Order::create($data);
            $total_order_summ = 300;
            $order_products = [];
            foreach ($order->products() as $product) {
                try {
                    $order_products[] = [
                        'user_id' => $product->product->page->user->id,
                        'order_id' => $order->id,
                    ];
                    $total_order_summ += $product->price * $product->count;
                } catch (\Exception $ex) {
                }
            }
            DB::table('order_user')->insert($order_products);

            $robokassa = new Robokassa($order->id, "Оплата заказа на сайте Blogger-store.ru", $total_order_summ);
            return $robokassa->payment();
        }

        $cart_products = self::products();
        $products = PageProduct::with(['product', 'product.images', 'product.sizes'])->find(array_keys($cart_products));
        if ($products->count() < 1)
            return redirect(route('cart'));
        return view('checkout', ['cart_products' => $cart_products, 'products' => $products]);
    }

    public function checkoutThanks(Request $request, $id = 0)
    {
        if ($request->isMethod('POST')) {
            if(env('ROBOKASSA_TEST_MODE')) {
                $p = env('ROBOKASSA_PASSWORD_TEST_1');
            }
            else
                $p = env('ROBOKASSA_PASSWORD_1');
            $t_crc = strtoupper(md5("$request->out_summ:$request->inv_id:$p"));
            if ($t_crc == strtoupper($request->SignatureValue)) {
                $order = Order::findOrFail($request->InvId);
                $order->order_status_id = 1;
                $order->save();
                TelegramBot::sendMessage('На сайт поступил новый заказ');
                return redirect(route('checkout.thanks', $request->InvId))->withCookie('cart', json_encode([]), 10080);
            } else {
                return 'ошибка';
            }
        }

        return view('checkout_thanks', ['order_id' => $id]);
    }
}
