<?php

namespace App\Http\Controllers;

use App\Category;
use App\File;
use App\InstagramApi;
use App\PageProduct;
use App\Filters\PageProductFilter;
use App\Product;
use App\Slide;
use App\Subscribe;
use App\SubscribeType;
use App\UserPage;
use App\YoutubeApi;
use http\Env\Response;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function index()
    {
        $new_products = PageProduct::orderBy('id', 'desc')->with(['product', 'product.images', 'product.page'])
            ->whereHas('product', function ($query) {
                $query->whereNotIn('category_id', [-1, -2, -3]);
            })->limit(12)->get();
        $categories = Category::whereNotIn('id', [-1, -2, -3])->limit(8)->get();
        $pages = UserPage::limit(8)->get();
        $slides = Slide::all();
        return view('index', ['new_products' => $new_products, 'categories' => $categories,
            'pages' => $pages, 'slides' => $slides]);
    }

    public function products(Request $request)
    {
        $categories = Category::all();

        $filter = PageProduct::where('id', '>', '0');
        $filter = (new PageProductFilter($filter, $request))->apply();
        $products = $filter->with(['product', 'product.images', 'product.page'])->paginate(20);
        $minMaxPrice = [
            'min' => $filter->min('price'),
            'max' => $filter->max('price'),
        ];

        $pages = UserPage::all();
        return view('category', ['categories' => $categories,
            'products' => $products, 'pages' => $pages, 'minMaxPrice' => $minMaxPrice]);
    }

    public function search(Request $request)
    {
        return $this->products($request);
    }

    public static function getPages()
    {
        return UserPage::limit(8)->get();
    }

    public function page(Request $request, $id)
    {
        $page = UserPage::with(['pageProducts', 'pageProducts.product'])->where('id', $id)->orWhere('slug', 'like', "%$id%")->first();
        if ($page->id == $id) {
            return redirect(route('page', $page->slug));
        }
        $products = $page->pageProducts->groupBy('product.category_id');
        $categories = Category::find($page->pageProducts->pluck('product.category_id'));

        return view('user_page', ['page' => $page, 'products' => $products, 'categories' => $categories]);
    }

    public function pageSocial(Request $request, $id)
    {
        $page = UserPage::where('id', $id)->orWhere('slug', 'like', "%$id%")->first();
        if ($page->social_id == 1) {
            //youtube
            $youtube = new YoutubeApi();
            $posts = $youtube->getChannelLastVideos($page->url);
        } else if ($page->social_id == 2) {
            //insta
            $inst = new InstagramApi();
            $inst = $inst->getUserInfo($page->url);
            $posts = $inst->graphql->user->edge_owner_to_timeline_media->edges;
//            dd($posts);
        }
        return view('partials.youtube_cards', ['posts' => $posts, 'type' => $page->social_id]);
    }

    public function pages(Request $request)
    {
        $pages = UserPage::all();
        return view('pages', ['pages' => $pages]);
    }

    public function modalProduct(Request $request)
    {
        $product = PageProduct::with(['product', 'product.images', 'product.page', 'product.sizes'])->findOrFail($request->product_id);
        return view('partials.product_modal', ['product' => $product]);
    }

    public function slides()
    {
        $slides = Slide::paginate(10);
        return view('admin.slides', ['slides' => $slides]);
    }

    public function slideEdit(Request $request, $id = null)
    {
        if ($id)
            $slide = Slide::find($id);
        else
            $slide = new Slide();

        if ($request->isMethod('POST')) {
            if ($request->image) {
                $slide->path = File::upload($request->image, '/slides')->path;
            }
            $slide->link = $request->link;
            $slide->save();

            return redirect(route('admin.slide.edit', $slide->id));
        }

        return view('admin.slide_edit', ['slide' => $slide]);
    }

    public function slideDelete(Request $request, $id)
    {
        $slides = Slide::find($id)->delete();
        return redirect(route('admin.slides'));
    }

    public function about()
    {
        return view('about');
    }

    public function contacts()
    {
        return view('contacts');
    }

    public function questions()
    {
        return view('questions');
    }

    public function job()
    {
        return view('job');
    }

    public function return()
    {
        return view('return');
    }

    public function pageSubscribe()
    {
        $pages = UserPage::all();
        return view('subscribe', ['pages' => $pages]);
    }

    public function subscribes(Request $request)
    {
        $products = Product::whereIn('category_id', [-1, -2, -3])->where('user_page_id', $request->page_id)->get();
        $subscribes = PageProduct::with(['product', 'product.images', 'product.page', 'product.sizes'])->whereIn('product_id', array_pluck($products, 'id'))->get();
//        return response()->json($subscribes);
        return view('partials.subscribe_modal', ['subscribes' => $subscribes]);
    }
}
