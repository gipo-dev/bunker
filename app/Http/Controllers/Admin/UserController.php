<?php

namespace App\Http\Controllers\Admin;

use App\File;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function list() {
        $users = User::where('role_id', 1)->with('page')->paginate(20);
        return view('admin.users', ['users' => $users]);
    }

    public function user(Request $request, $id) {
        $user = User::with(['transactions.type', 'page'])->findOrFail($id);
        $user->orders = $user->orders()->orderBy('id', 'desc')->paginate(5, ['*'], 'orders');
        $transactions = $user->transactions()->orderBy('id', 'desc')->paginate(10);
        return view('admin.user', ['user' => $user, 'transactions' => $transactions]);
    }

    public function edit(Request $request, $id = null) {
        if($id == 'new') {
            $user = new User();
            $user->role_id = 1;
            $user->balance = encrypt(0);
        } else
            $user = User::findOrFail($id);

        if($request->isMethod('POST')) {
            $user->name = $request->name;
            $user->email = $request->email;
            if($request->avatar) {
                $user->avatar = File::upload($request->avatar, '/avatars')->path;
            }
            if($request->password) {
                $user->password = bcrypt($request->password);
            }
            $user->save();
            return redirect(route('admin.user', $user->id));
        }

        return view('admin.user_edit', ['user' => $user]);
    }

    public function delete(Request $request, $id) {
        User::findOrFail($id)->delete();
        return redirect(route('admin.users'));
    }

    public function userAddBalance(Request $request, $id) {
        $user = User::findOrFail($id);
        $user->wallet()->add($request->summ, $request->all(), 2);
        return redirect(route('admin.user', $id));
    }
}
