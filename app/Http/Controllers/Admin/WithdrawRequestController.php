<?php

namespace App\Http\Controllers\Admin;

use App\WithdrawRequest;
use App\WithdrawRequestStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WithdrawRequestController extends Controller
{
    public function list() {
        $requests = WithdrawRequest::orderBy('id', 'desc')->with(['status', 'user'])->paginate(20);
        return view('admin.withdrawRequests', ['requests' => $requests]);
    }

    public function edit(Request $request, $id) {
        $withdrawRequest = WithdrawRequest::with(['status', 'user'])->find($id);

        if($request->isMethod('POST')) {
            if($request->status_id == 2 && $withdrawRequest->state == 0) {
                $withdrawRequest->user->wallet()->add( ($withdrawRequest->summ * -1), $withdrawRequest, 3 );
                $withdrawRequest->state = 1;
            }
            $withdrawRequest->comment = $request->comment;
            $withdrawRequest->status_id = $request->status_id;
            $withdrawRequest->save();
            return redirect(route('admin.withdrawRequest.edit', $id));
        }

        $statuses = WithdrawRequestStatus::all();
        return view('admin.withdrawRequest_edit', ['withdrawRequest' => $withdrawRequest, 'statuses' => $statuses]);
    }
}
