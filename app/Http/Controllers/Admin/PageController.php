<?php

namespace App\Http\Controllers\Admin;

use App\File;
use App\User;
use App\UserPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function list() {
        $pages = UserPage::with('user')->paginate(20);
        return view('admin.pages', ['pages' => $pages]);
    }

    public function edit(Request $request, $id = null) {
        if(!$id)
            $page = new UserPage();
        else
            $page = UserPage::with('user')->find($id);

        if($request->isMethod('POST')) {
            $page->user_id = $request->user_id;
            $page->footer_title = $request->footer_title;
            $page->footer_text = $request->footer_text;
            $page->slug = $request->slug;
            $page->social_id = $request->social_id;
            $page->url = $request->link;

            if($request->small_image) {
                $page->small_image = File::upload($request->small_image, '/pages')->path;
            }
            if($request->header_image) {
                $page->header_image = File::upload($request->header_image, '/pages')->path;
            }
            if($request->footer_image) {
                $page->footer_image = File::upload($request->footer_image, '/pages')->path;
            }
            $page->save();

            return redirect(route('admin.pages'));
        }

        $users = User::where('role_id', 1)->get();
//        dd($page);
        return view('admin.page_edit', ['page' => $page, 'users' => $users]);
    }

    public function delete(Request $request, $id) {
        UserPage::find($id)->delete();
        return redirect(route('admin.pages'));
    }
}
