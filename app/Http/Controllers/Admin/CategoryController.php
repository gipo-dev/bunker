<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\ProductSizeType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function list() {
        $categories = Category::paginate(20);
        return view('admin.categories', ['categories' => $categories]);
    }

    public function edit(Request $request, $id = null) {
        if(!$id)
            $category = new Category();
        else
            $category = Category::find($id);

        if($request->isMethod('POST')) {
            $category->name = $request->name;
            $category->slug = $request->slug;
            $category->size_type_id = $request->size_type_id;
            $category->save();
            return redirect(route('admin.categories'));
        }

        $size_types = ProductSizeType::all();

        return view('admin.category_edit', ['category' => $category, 'size_types' => $size_types]);
    }

    public function delete(Request $request, $id) {
        Category::find($id)->delete();
        return redirect(route('admin.categories'));
    }
}
