<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\OrderHistory;
use App\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function list()
    {
        $orders = Order::with('status')->orderBy('id', 'desc')->paginate(20);
        return view('admin.orders', ['orders' => $orders]);
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('POST')) {
            $order = Order::findOrFail($id);
            $order->order_status_id = $request->order_status_id;
            $order->save();

            if ($request->order_status_id == 4 && $order->state == 0) {
                $order = Order::findOrFail($id);
                foreach ($order->products() as $product) {
                    $product->product->page->user->wallet()->add(($product->price - $product->product->base_price) * $product->count, $order);
                }
                $order->state = 1;
                $order->save();
            }

            $orderHistory = new OrderHistory();
            $orderHistory->order_id = $id;
            $orderHistory->order_status_id = $request->order_status_id;
            $orderHistory->comment = $request->comment;
            $orderHistory->save();

            return redirect(route('admin.order.edit', $id));
        }

        $order = Order::with(['status'])->findOrFail($id);
        $statuses = OrderStatus::all();
        return view('admin.order_edit', ['order' => $order, 'statuses' => $statuses]);
    }

    public function print(Request $request, $id)
    {
        $order = Order::with(['status'])->findOrFail($id);
        return view('admin.order_print', ['order' => $order]);
    }
}
