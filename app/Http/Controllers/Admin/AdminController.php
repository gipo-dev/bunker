<?php

namespace App\Http\Controllers\Admin;

use App\Telegram\TelegramBot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index() {
        return view('admin.index');
    }

    public static function getLMenu()
    {
        return [
            [
                'name' => 'Главная',
                'link' => route('admin'),
                'icon' => 'home',
            ],
            [
                'name' => 'Пользователи',
                'link' => route('admin.users'),
                'icon' => 'user',
            ],
            [
                'name' => 'Страницы',
                'link' => route('admin.pages'),
                'icon' => 'globe',
            ],
            [
                'name' => 'Товары',
                'link' => 'products',
                'icon' => 'box',
                'items' => [
                    [
                        'name' => 'Категории',
                        'link' => route('admin.categories'),
                        'icon' => 'stream',
                    ],
                    [
                        'name' => 'Товары',
                        'link' => route('admin.products'),
                        'icon' => 'box',
                    ],
                ]
            ],
            [
                'name' => 'Заказы',
                'link' => route('admin.orders'),
                'icon' => 'shopping-cart',
            ],
            [
                'name' => 'Запросы средств',
                'link' => route('admin.withdrawRequests'),
                'icon' => 'money-check',
            ],
            [
                'name' => 'Слайды',
                'link' => route('admin.slides'),
                'icon' => 'image',
            ],
            [
                'name' => 'Обратная связь',
                'link' => route('admin.feedbacks'),
                'icon' => 'comment',
            ],
        ];
    }
}
