<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\File;
use App\PageProduct;
use App\Product;
use App\ProductImage;
use App\ProductSize;
use App\Size;
use App\UserPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function list()
    {
        $products = Product::with('page')->with('images')->with('category')->paginate(20);
        return view('admin.products', ['products' => $products]);
    }

    public function edit(Request $request, $id = null)
    {
        if (!$id)
            $product = new Product();
        else
            $product = Product::where('id', $id)->with('sizes')->with('category')->first();

        $product->sizes = $product->sizes->keyBy('size_id');
        if ($request->isMethod('POST')) {
            $product->name = $request->name;
            $product->category_id = $request->category_id;
            $product->base_price = $request->base_price;
            $product->description = $request->description;
            $product->user_page_id = $request->user_page_id;
            if (!$product->slug)
                $product->generateSlug();
            if ($request->sizes) {
                $sizes = array();
                foreach ($request->sizes as $k => $size)
                    $sizes[] = [
                        'product_id' => $product->id,
                        'size_id' => $k,
                        'count' => $size,
                    ];
                ProductSize::where('product_id', $product->id)->delete();
                ProductSize::insert($sizes);
            } else if(isset($request->count)) {
                $product->count = $request->count;
            }
            unset($product->sizes);
            $product->save();

            if($id)
                return redirect(route('admin.products'));
            else
                return redirect(route('admin.product.edit', $product->id));
        }

        $categories = Category::all();
        $pages = UserPage::all();
        if (isset($product->category))
            $sizes = Size::where('product_size_type_id', $product->category->size_type_id)->get();
        else
            $sizes = [];

        return view('admin.product_edit', ['product' => $product, 'categories' => $categories,
            'pages' => $pages, 'sizes' => $sizes]);
    }

    public function delete(Request $request, $id)
    {
        PageProduct::where('product_id', $id)->delete();
        Product::find($id)->delete();
        return redirect(route('admin.products'));
    }

    public function addImage(Request $request, $id) {
        $image = new ProductImage();
        $image->product_id = $id;
        $image->path = File::upload($request->image, '/products')->path;
        $image->save();
        return response()->json($image);
    }

    public function deleteImage(Request $request, $id) {
        ProductImage::find($id)->delete();
        return response()->json(['ok']);
    }
}
