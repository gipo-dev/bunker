<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\FeedbackCategory;
use App\FeedbackStatus;
use App\File;
use App\Telegram\TelegramBot;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index(Request $request)
    {
        $categories = FeedbackCategory::all();
        return view('feedback', ['categories' => $categories]);
    }

    public function add(Request $request)
    {
        $feed = new Feedback();
        $feed->fill($request->all());
        $feed->status_id = 1;
        if (count($request->files->all()) > 0) {
            $_f = [];
            foreach ($request->files->all()['files'] as $file)
//                dd($request->files);
                $_f[] = File::upload($file, '/feedback')->path;
            $feed->files = json_encode($_f);
        }
        $feed->save();
        TelegramBot::sendMessage("В службу поддрежки поступило новое сообщение. Тема: ". $request->subject);
        return redirect(route('feedback'))->with('status', 'Ваш вопрос успешно отправлен в службу поддержки.');
    }

    public function list()
    {
        $messages = Feedback::with('status', 'category')->orderBy('id', 'desc')->paginate();
        return view('admin.feedbacks', ['messages' => $messages]);
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('POST')) {
            $fb = Feedback::find($id);
            $fb->status_id = $request->status_id;
            $fb->save();
            return redirect(route('admin.feedbacks'));
        }

        $fb = Feedback::with(['status', 'category'])->find($id);
        $statuses = FeedbackStatus::all();
        return view('admin.feedback_edit', ['feedback' => $fb, 'statuses' => $statuses]);
    }
}
