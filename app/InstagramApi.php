<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class InstagramApi extends Model
{
    public function getUserInfo($user_name)
    {
        if (!$user_name)
            return;
        return $this->sendRequest("https://www.instagram.com/$user_name/?__a=1");
    }

    public function sendRequest($url, $data = [], $method = 'GET')
    {
        $client = new Client();
        try {
            $resp = $client->request($method, $url, [
                'form_params' => $data,
            ])->getBody()->getContents();
        } catch (\Exception $ex) {
            $resp = '';
        }

        return json_decode($resp);
    }
}
