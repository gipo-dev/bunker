<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class UserWallet extends Model
{
    private $user;

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    public function getBalanceAttribute() {
        return decrypt($this->user->balance);
    }

    public function add(int $summ, $data, $transaction_type_id = 1) {
        $transaction = new Transaction();
        $transaction->transaction_type_id = $transaction_type_id;
        $transaction->user_id = $this->user->id;
        $transaction->transaction_data = [
            'user' => $this->user->id,
            'initiator' => [
                'id' => Auth::user()->id,
                'email' => Auth::user()->email,
            ],
            'original' => json_encode($transaction),
            'transaction' => json_encode($data),
            'summ' => $summ,
            'date' => Carbon::now()
        ];
        $transaction->save();

        $user = User::findOrFail($this->user->id);
        $_balance = decrypt($user->balance);
        $_balance += $summ;
        $user->balance = encrypt($_balance);
        $user->save();
    }
}
