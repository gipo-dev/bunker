/* =================================
------------------------------------
	Divisima | eCommerce Template
	Version: 1.0
 ------------------------------------
 ====================================*/


'use strict';


$(window).on('load', function () {
    /*------------------
        Preloder
    --------------------*/
    $(".loader").fadeOut();
    $("#preloder").delay(400).fadeOut("slow");

});

(function ($) {
    /*------------------
        Navigation
    --------------------*/
    $('#t-menu').slicknav({
        prependTo: '.main-navbar .container',
        closedSymbol: '<i class="flaticon-right-arrow"></i>',
        openedSymbol: '<i class="flaticon-down-arrow"></i>'
    });


    /*------------------
        ScrollBar
    --------------------*/
    $(".cart-table-warp, .product-thumbs").niceScroll({
        cursorborder: "",
        cursorcolor: "#afafaf",
        boxzoom: false
    });


    /*------------------
        Category menu
    --------------------*/
    $('.category-menu > li').hover(function (e) {
        $(this).addClass('active');
        e.preventDefault();
    });
    $('.category-menu').mouseleave(function (e) {
        $('.category-menu li').removeClass('active');
        e.preventDefault();
    });


    /*------------------
        Background Set
    --------------------*/
    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });


    /*------------------
        Hero Slider
    --------------------*/
    var hero_s = $(".hero-slider");
    hero_s.owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        dots: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
        smartSpeed: 1200,
        autoHeight: true,
        autoplay: true,
        onInitialized: function () {
            var a = this.items().length;
            $("#snh-1").html("<span>1</span><span>" + a + "</span>");
        }
    }).on("changed.owl.carousel", function (a) {
        var b = --a.item.index, a = a.item.count;
        $("#snh-1").html("<span> " + (1 > b ? b + a : b > a ? b - a : b) + "</span><span>" + a + "</span>");

    });

    hero_s.append('<div class="slider-nav-warp"><div class="slider-nav"></div></div>');
    $(".hero-slider .owl-nav, .hero-slider .owl-dots").appendTo('.slider-nav');


    /*------------------
        Brands Slider
    --------------------*/
    $('.product-slider').owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        margin: 30,
        autoplay: true,
        navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 2,
            },
            768: {
                items: 3,
            },
            1200: {
                items: 4,
            }
        }
    });


    /*------------------
        Popular Services
    --------------------*/
    $('.popular-services-slider').owlCarousel({
        loop: true,
        dots: false,
        margin: 40,
        autoplay: true,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            991: {
                items: 3
            }
        }
    });


    /*------------------
        Accordions
    --------------------*/
    $('.panel-link').on('click', function (e) {
        $('.panel-link').removeClass('active');
        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.addClass('active');
        }
        e.preventDefault();
    });


    /*-------------------
        Range Slider
    --------------------- */
    var rangeSlider = $(".price-range"),
        minamount = $("#minamount"),
        maxamount = $("#maxamount"),
        minPrice = rangeSlider.data('min'),
        maxPrice = rangeSlider.data('max');
    rangeSlider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            minamount.val(ui.values[0] + 'руб.');
            maxamount.val(ui.values[1] + 'руб.');
        }
    });
    minamount.val(rangeSlider.slider("values", 0) + 'руб.');
    maxamount.val(rangeSlider.slider("values", 1) + 'руб.');


    /*-------------------
        Quantity change
    --------------------- */
    var proQty = $('.pro-qty');
    proQty.prepend('<span class="dec qtybtn">-</span>');
    proQty.append('<span class="inc qtybtn">+</span>');
    proQty.on('click', '.qtybtn', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find('input').val(newVal);
    });


    /*------------------
        Single Product
    --------------------*/
    $('.product-thumbs-track > .pt').on('click', function () {
        $('.product-thumbs-track .pt').removeClass('active');
        $(this).addClass('active');
        var imgurl = $(this).data('imgbigurl');
        var bigImg = $('.product-big-img').attr('src');
        if (imgurl != bigImg) {
            $('.product-big-img').attr({src: imgurl});
            $('.zoomImg').attr({src: imgurl});
        }
    });


    $('.product-pic-zoom').zoom();

    $('#bloggers-menu, .blogger-container').hover(function () {
        $('.blogger-container').addClass('show');
    });
    $('.blogger-container').mouseleave(function () {
        $('.blogger-container').removeClass('show');
    });

    $('#to-basket-form').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            url: '/cart/add',
            data: data,
            success: function (data) {
                // el.toggleClass('active');
                $('#to-basket-btn').toggleClass('sb-dark');
            },
            error: function (data) {
                console.log(data);
                alert('Ошибка добавления товара');
            }
        });
    });
    $('.add-card').click(function () {
        var el = $(this);
        if (el.hasClass('active')) {
            $.ajax({
                url: '/cart/add',
                data: {
                    product_id: el.data('product-id'),
                },
                success: function (data) {
                    el.toggleClass('active');
                    el.find('.text').text('В КОРЗИНУ');
                },
                error: function (data) {
                    console.log(data);
                    alert('Ошибка добавления товара');
                }
            });
        } else {
            getModalProduct(el.data('product-id'));
        }
    });

    $('.product-item').click(function (e) {
        console.log('a');
        if ($(e.target).is('a') || $(e.target).is('i') || $(e.target).is('span.text')) {
            e.preventDefault();
            return;
        }
        getModalProduct($(this).data('product-id'));
    });
    $('#sub-form').submit(function (e) {
        e.preventDefault();
        getModalProduct($(this).data('product-id'), 'subscribe', $(this).serialize());
    });

    $('.wishlist-btn').click(function () {
        var el = $(this);
        $.ajax({
            url: '/favorites/add',
            data: {
                product_id: el.data('product-id'),
            },
            success: function (data) {
                el.toggleClass('active');
            },
            error: function (data) {
                console.log(data);
                alert('Ошибка добавления товара');
            }
        });
    });

    $('#header-top').mouseenter(function () {
        $('.blogger-container').removeClass('show');
    });

    $('.custom-file-input').on('change', function (e) {
        //get the file name
        var files = Array.from(e.target.files);
        var name = [];
        files.forEach(function (file, i) {
            name.push(file.name);
        });
        $(this).next('.custom-file-label').html(name.join(', '));
    });

    $(document).on('scroll', function (e) {
        if ($(document).scrollTop() > 200) {
            $('#header-top').addClass('header-top-fixed').addClass('shadow-sm');
            var elements = $('#t-menu li').detach();
            $('#to-fixed-menu').append(elements);
            $('.site-logo img').attr('src', '/img/logo-light.png');
        } else {
            $('#header-top').removeClass('header-top-fixed').removeClass('shadow-sm');
            var elements = $('#to-fixed-menu li').detach();
            $('#t-menu').append(elements);
            $('.site-logo img').attr('src', '/img/logo.png');
        }
    });


})(jQuery);

function getModalProduct(product_id, type = 'product', dat = {}) {

    if(type == 'product')
        var uri = '/modal-product';
    else
        var uri = '/subscribe/generate';
    dat.product_id = product_id;
    $.ajax({
        url: uri,
        data: dat,
        success: function (data) {
            $('#modal-product-content').html(data);

            $('#product-modal').modal('show');

            $('.product-thumbs-track > .pt').on('click', function () {
                $('.product-thumbs-track .pt').removeClass('active');
                $(this).addClass('active');
                var imgurl = $(this).data('imgbigurl');
                var bigImg = $('.product-big-img').attr('src');
                if (imgurl != bigImg) {
                    $('.product-big-img').attr({src: imgurl});
                    $('.zoomImg').attr({src: imgurl});
                }
            });
            $('.product-pic-zoom').zoom();

            var proQty = $('.pro-qty');
            proQty.prepend('<span class="dec qtybtn">-</span>');
            proQty.append('<span class="inc qtybtn">+</span>');
            proQty.on('click', '.qtybtn', function () {
                var $button = $(this);
                var oldValue = $button.parent().find('input').val();
                if ($button.hasClass('inc')) {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 1) {
                        var newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                $button.parent().find('input').val(newVal);
            });

            $('#to-basket-form').on('submit', function (e) {
                e.preventDefault();
                var form = $(this).serialize();
                $.ajax({
                    url: '/cart/add',
                    data: form,
                    success: function (data) {
                        // el.toggleClass('active');
                        $('#to-basket-btn').toggleClass('sb-dark');
                        $('#cart-count').text(data);
                        $('#to-basket-btn').text('В корзине');
                        var prod = $(".add-card[data-product-id='" + form.product_id + "']");
                        prod.toggleClass('active');
                        prod.find('.text').text('В КОРЗИНЕ');
                        $('#cart-count').text(data);
                    },
                    error: function (data) {
                        console.log(data);
                        alert('Ошибка добавления товара');
                    }
                });
            });
        },
        error: function (data) {
            console.log(data);
            alert('Ошибка');
        }
    });
}
